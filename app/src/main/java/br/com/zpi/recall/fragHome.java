package br.com.zpi.recall;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import br.com.zpi.recall.db.RCDBException;
import br.com.zpi.recall.db.dbBlock;
import br.com.zpi.recall.db.dbRecord;
import br.com.zpi.recall.model.modContact;
import br.com.zpi.recall.model.modRecord;
import br.com.zpi.recall.tools.Convert;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
public class fragHome extends Fragment {
    private actContacts myActivity = null;
    private RelativeLayout layReply = null;
    private RelativeLayout layHReply = null;
    private Spinner spinReply = null;

    public fragHome() {
        // Required empty public constructor
    }


    //##############################################################################################
    // ON CREATE
    //##############################################################################################

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }


    //##############################################################################################
    // VIEW CREATED
    //##############################################################################################

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myActivity = (actContacts) getActivity();

        Button btnHBlist = (Button) getActivity().findViewById(R.id.btnHBlist);
        Button btnHwlist = (Button) getActivity().findViewById(R.id.btnHwlist);
        TextView txtPPlink = (TextView) getActivity().findViewById(R.id.txtPPlink);
        spinReply = (Spinner) getActivity().findViewById(R.id.spinReply);
        EditText inpReply = (EditText) getActivity().findViewById(R.id.inpReply);
        layReply = (RelativeLayout) getActivity().findViewById(R.id.layReply);
        layHReply = (RelativeLayout) getActivity().findViewById(R.id.layHReply);


        ArrayAdapter a = new ArrayAdapter<String>(getActivity(), R.layout.spin_white, getActivity().getResources().getStringArray(R.array.reply_opt));
        spinReply.setAdapter(a);
        spinReply.setSelection(myActivity.prefs.getInt("replyMsg", 0));
        inpReply.setEnabled(false);
        inpReply.setTextColor(getActivity().getResources().getColor(R.color.rc_white70));
        if (myActivity.prefs.getInt("replyMsg", 0) > 6) {
            inpReply.setEnabled(true);
            inpReply.setTextColor(getActivity().getResources().getColor(R.color.rc_white));
        }
        spinReply.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                EditText inpReply = (EditText) getActivity().findViewById(R.id.inpReply);

                myActivity.prefeditor.remove("replyMsg");
                myActivity.prefeditor.putInt("replyMsg", position);
                myActivity.prefeditor.commit();

                if (position < 6) {
                    inpReply.setTextColor(getActivity().getResources().getColor(R.color.rc_white70));
                    inpReply.setEnabled(false);
                    inpReply.setText(getActivity().getResources().getStringArray(R.array.reply_txt)[position]);
                } else {
                    inpReply.setEnabled(true);
                    inpReply.setText(myActivity.prefs.getString("customReply", "").trim());
                    inpReply.setTextColor(getActivity().getResources().getColor(R.color.rc_white));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });


        inpReply.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (spinReply.getSelectedItemPosition() >= 6) {
                    myActivity.prefeditor.remove("customReply");
                    myActivity.prefeditor.putString("customReply", s.toString());
                    myActivity.prefeditor.commit();
                }
            }
        });


        Drawable dico = getResources().getDrawable(R.drawable.ico_edit);
        dico.setBounds(0, 0, (int) (dico.getIntrinsicWidth() * 0.2),
                (int) (dico.getIntrinsicHeight() * 0.2));
        btnHBlist.setCompoundDrawablePadding(Convert.dpToPx(10, getActivity()));
        btnHBlist.setCompoundDrawables(null, null, dico, null);

        btnHwlist.setCompoundDrawablePadding(Convert.dpToPx(10, getActivity()));
        btnHwlist.setCompoundDrawables(null, null, dico, null);

        txtPPlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(myActivity, actPolicy.class);
                startActivityForResult(i, 11);
            }
        });

    }

    //##############################################################################################
    // ON RESUME
    //##############################################################################################

    @Override
    public void onResume() {
        super.onResume();

        CheckBox ckBlocker = (CheckBox) getActivity().findViewById(R.id.ckBlocker);
        CheckBox ckRec = (CheckBox) getActivity().findViewById(R.id.ckRec);
        CheckBox ckNotif = (CheckBox) getActivity().findViewById(R.id.ckNotif);
        CheckBox ckBlist = (CheckBox) getActivity().findViewById(R.id.ckBlist);
        CheckBox ckBwlist = (CheckBox) getActivity().findViewById(R.id.ckBwlist);
        CheckBox ckHGDrive = (CheckBox) getActivity().findViewById(R.id.ckHGDrive);
        CheckBox ckHReply = (CheckBox) getActivity().findViewById(R.id.ckHReply);

        Button btnHBlist = (Button) getActivity().findViewById(R.id.btnHBlist);
        Button btnHwlist = (Button) getActivity().findViewById(R.id.btnHwlist);
        TextView txtHSBlock = (TextView) getActivity().findViewById(R.id.txtHSBlock);
        TextView txtHSRec = (TextView) getActivity().findViewById(R.id.txtHSRec);
        dbBlock dbB = new dbBlock(getActivity());
        dbRecord dbR = new dbRecord(getActivity());

        layReply.setVisibility(View.GONE);
        layHReply.setVisibility(View.GONE);

        try {
            modContact[] b = dbB.listLogs(null, null, 0, 0);
            if (b != null)
                txtHSBlock.setText(String.valueOf(b.length));
        } catch (RCDBException e) {

        }

        try {
            modRecord[] r = dbR.getRecords(null);
            if (r != null)
                txtHSRec.setText(String.valueOf(r.length));
        } catch (Exception e) {

        }


        // CHECKBOX BLOCKER
        if (myActivity.prefs.getString("blockall", "").trim().toUpperCase().equals("X")) {
            ckBlocker.setChecked(true);
            layHReply.setVisibility(View.VISIBLE);
        } else {
            layHReply.setVisibility(View.GONE);
            ckBlocker.setChecked(false);
        }

        ckBlocker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    myActivity.prefeditor.remove("blockall");
                    layHReply.setVisibility(View.GONE);
                    myActivity.prefeditor.commit();
                    myActivity.myService.refreshNotification();
                    myActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_blockall_off).toString(), 2);
                } else {
                    myActivity.prefeditor.putString("blockall", "X");
                    layHReply.setVisibility(View.VISIBLE);
                    myActivity.prefeditor.commit();
                    myActivity.myService.refreshNotification();
                    myActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_blockall_on).toString(), 2);
                }

            }
        });

        // CHECKBOX REC
        if (myActivity.prefs.getString("recall", "").trim().toUpperCase().equals("X"))
            ckRec.setChecked(true);
        else
            ckRec.setChecked(false);

        ckRec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    myActivity.prefeditor.remove("recall");
                    myActivity.prefeditor.commit();
                    myActivity.myService.refreshNotification();
                    myActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_recall_off).toString(), 2);
                } else {
                    myActivity.prefeditor.putString("recall", "X");
                    myActivity.prefeditor.commit();
                    myActivity.myService.refreshNotification();
                    myActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_recall_on).toString(), 2);
                }

            }
        });


        // CHECKBOX NOTIFICATIONS
        if (myActivity.prefs.getString("enablenotif", "").trim().toUpperCase().equals("X"))
            ckNotif.setChecked(true);
        else
            ckNotif.setChecked(false);

        ckNotif.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    myActivity.prefeditor.remove("enablenotif");
                    myActivity.prefeditor.commit();
                    myActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_notif_off).toString(), 2);
                } else {
                    myActivity.prefeditor.putString("enablenotif", "X");
                    myActivity.prefeditor.commit();
                    myActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_notif_on).toString(), 2);
                }

            }
        });


        // AUTOREPLY
        if (myActivity.prefs.getString("reply", "").trim().toUpperCase().equals("X")) {
            ckHReply.setChecked(true);
            layReply.setVisibility(View.VISIBLE);
        } else {
            layReply.setVisibility(View.GONE);
            ckHReply.setChecked(false);
        }
        ckHReply.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    myActivity.prefeditor.remove("reply");
                    myActivity.prefeditor.commit();
                    layReply.setVisibility(View.GONE);
                    myActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_reply_off).toString(), 2);
                } else {
                    myActivity.prefeditor.putString("reply", "X");
                    myActivity.prefeditor.commit();
                    layReply.setVisibility(View.VISIBLE);
                    myActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_reply_on).toString(), 6);
                }

            }
        });

        // BLACK LIST
        if (myActivity.prefs.getString("blocklist", "").trim().toUpperCase().equals("X"))
            ckBlist.setChecked(true);
        else
            ckBlist.setChecked(false);

        ckBlist.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    myActivity.prefeditor.remove("blocklist");
                    myActivity.prefeditor.commit();
                } else {
                    myActivity.prefeditor.putString("blocklist", "X");
                    myActivity.prefeditor.commit();
                }

            }
        });


        // WHITE LIST
        if (myActivity.prefs.getString("allowlist", "").trim().toUpperCase().equals("X"))
            ckBwlist.setChecked(true);
        else
            ckBwlist.setChecked(false);

        ckBwlist.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    myActivity.prefeditor.remove("allowlist");
                    myActivity.prefeditor.commit();
                } else {
                    myActivity.prefeditor.putString("allowlist", "X");
                    myActivity.prefeditor.commit();
                }

            }
        });


        // CHECKBOX GOOGLE DRIVE
        if (myActivity.prefs.getString("gdrive", "").trim().toUpperCase().equals("X"))
            ckHGDrive.setChecked(true);
        else
            ckHGDrive.setChecked(false);

        // GOOGLE DRIVE INTEGRATION
        ckHGDrive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    myActivity.prefeditor.remove("gdrive");
                    myActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.home_info_gdriveoff).toString(), 2);
                    myActivity.prefeditor.commit();
                    myActivity.disableGDrive();
                } else {
                    myActivity.prefeditor.putString("gdrive", "X");
                    myActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.home_info_gdriveon).toString(), 2);
                    myActivity.prefeditor.commit();
                    myActivity.enableGDrive();
                    myActivity.connectGDrive();
                }
            }
        });


        // BLACK LIST
        btnHBlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), actBlocked.class);
                startActivity(i);
            }
        });


        // WHITE LIST
        btnHwlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), actAllowed.class);
                startActivity(i);
            }
        });
    }


}
