package br.com.zpi.recall.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import br.com.zpi.recall.R;
import br.com.zpi.recall.model.modContact;
import br.com.zpi.recall.tools.RCContacts;

	/*
	 * Copyright 2018 Leonardo Germano Roese
	 *
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 *
	 * http://www.apache.org/licenses/LICENSE-2.0
	 *
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

public class dbBlock {

    private Context ctx = null;
    private DBHELPER dbh = null;

    public dbBlock(Context ctx) {
        this.ctx = ctx;
        this.dbh = new DBHELPER(this.ctx);
    }

    //----------------------------------------------------------------------------------------------
    // INSERT BLOCK LOG ENTRY
    //----------------------------------------------------------------------------------------------
    public boolean logNum(String number, String contactid, String contactname, String dtcall) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_informnumber).toString());

        if (dtcall == null || dtcall.trim().length() <= 0)
            dtcall = ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        number = number.replaceAll("[^0-9]", "");
        number = number.replaceAll(" ", "");

        SQLiteDatabase db = this.dbh.getWritableDatabase();

        ContentValues cv = new ContentValues(4);
        cv.put("number", number);
        cv.put("dtcall", dtcall);
        if (contactid != null && contactid.trim().length() > 0)
            cv.put("contactid", contactid);
        else
            cv.put("contactid", " ");
        if (contactname != null && contactname.trim().length() > 0)
            cv.put("contactname", contactname);
        else
            cv.put("contactname", " ");

        try {
            if (db.insert("blocked_log", "number, dtcall, contactid, contactname", cv) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_insert).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_insert).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // MARK AS VIEWED
    //----------------------------------------------------------------------------------------------
    public boolean setViewed(String number, String dtcall) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_informnumber).toString());

        if (dtcall == null || dtcall.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_informnumber).toString());

        number = number.replaceAll("[^0-9]", "");
        number = number.replaceAll(" ", "");

        SQLiteDatabase db = this.dbh.getWritableDatabase();

        ContentValues cv = new ContentValues(1);
        cv.put("dtview", ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        try {
            if (db.update("blocked_log", cv, " number = ? AND dtcall = ?", new String[]{number, dtcall}) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_insert).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_insert).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // MARK AS NOTIFICATION VIEWED
    //----------------------------------------------------------------------------------------------
    public boolean setNotifViewed() throws RCDBException {

        SQLiteDatabase db = this.dbh.getWritableDatabase();
        ContentValues cv = new ContentValues(1);
        cv.put("notif", "X");

        try {
            if (db.update("blocked_log", cv, null, null) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_insert).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_insert).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // GET ALL BLOCK LOGS
    //----------------------------------------------------------------------------------------------

    public modContact[] listLogs(String filter, String order, int limit, int pageIn) throws RCDBException {
        SQLiteDatabase db = this.dbh.getReadableDatabase();
        dbContacts dbC = new dbContacts(ctx);

        String q = "SELECT * FROM blocked_log ";
        if (filter != null && filter.trim().length() > 0)
            q = q + " WHERE number LIKE '%" + filter + "%' OR UPPER(contactname) LIKE '%" + filter.toUpperCase().trim() + "%' ";

        if (order != null && order.trim().length() > 0)
            q = q + " ORDER BY" + order;
        else
            q = q + " ORDER BY dtcall DESC ";
        if (limit > 0)
            q = q + " LIMIT " + pageIn + ", " + limit;

        try {
            Cursor cur = db.rawQuery(q, null);
            if (cur != null && cur.moveToFirst()) {
                modContact[] outlist = new modContact[cur.getCount()];
                for (int i = 0; i < cur.getCount(); i++) {
                    outlist[i] = new modContact();
                    if (cur.getString(0) != null)
                        outlist[i].phone = cur.getString(0);
                    if (cur.getString(1) != null)
                        outlist[i].dtcall = cur.getString(1);
                    if (cur.getString(2) != null)
                        outlist[i].cid = cur.getString(2);
                    if (cur.getString(3) != null)
                        outlist[i].name = cur.getString(3);
                    if (cur.getString(4) != null)
                        outlist[i].dtview = cur.getString(4);
                    if (cur.getString(5) != null)
                        outlist[i].notif = cur.getString(5);

                    modContact c = RCContacts.getContactByNumber(ctx, outlist[i].phone);
                    if (c != null) {
                        outlist[i].cid = c.cid;
                        outlist[i].name = c.name;
                        outlist[i].icon = c.icon;
                    }
                    cur.moveToNext();
                }
                return outlist;
            } else {
                return null;
            }

        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_list).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // GET SPECIFIC NUMBER BLOCK LOGS
    //----------------------------------------------------------------------------------------------

    public modContact[] listLogs(String number) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_informnumber).toString());

        number = number.replaceAll("[^0-9]", "");
        number = number.replaceAll(" ", "");
        if (number.length() > 9)
            number = number.substring(number.length() - 9);

        SQLiteDatabase db = this.dbh.getReadableDatabase();

        try {
            Cursor cur = db.query("blocked_log", new String[]{"number", "dtcall", "contactid", "dtview"}, "number LIKE ?", new String[]{"%" + number}, null, null, "dtcall DESC");
            if (cur != null && cur.moveToFirst()) {
                modContact[] outlist = new modContact[cur.getCount()];
                for (int i = 0; i < cur.getCount(); i++) {
                    outlist[i] = new modContact();
                    if (cur.getString(0) != null)
                        outlist[i].phone = cur.getString(0);
                    if (cur.getString(1) != null)
                        outlist[i].dtcall = cur.getString(1);
                    if (cur.getString(2) != null)
                        outlist[i].cid = cur.getString(2);
                    if (cur.getString(3) != null)
                        outlist[i].name = cur.getString(3);
                    if (cur.getString(4) != null)
                        outlist[i].dtview = cur.getString(4);
                    if (cur.getString(5) != null)
                        outlist[i].notif = cur.getString(5);
                    modContact c = RCContacts.getContactByNumber(ctx, outlist[i].phone);
                    if (c != null) {
                        outlist[i].name = c.name;
                        outlist[i].icon = c.icon;
                    }

                    cur.moveToNext();
                }
                return outlist;
            } else {
                return null;
            }

        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_list).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // GET SPECIFIC NUMBER BLOCKED
    //----------------------------------------------------------------------------------------------

    public modContact wasBlocked(String number, String dtcall, boolean cinfo) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_informnumber).toString());

        number = number.replaceAll("[^0-9]", "");
        number = number.replaceAll(" ", "");
        if (number.length() > 9)
            number = number.substring(number.length() - 9);

        SQLiteDatabase db = this.dbh.getReadableDatabase();

        try {
            Cursor cur = db.query("blocked_log", null, "number LIKE ? AND dtcall = ?", new String[]{"%" + number, dtcall.trim()}, null, null, "dtcall DESC");
            if (cur != null && cur.moveToFirst()) {
                modContact out = new modContact();
                if (cur.getString(0) != null)
                    out.phone = cur.getString(0);
                if (cur.getString(1) != null)
                    out.dtcall = cur.getString(1);
                if (cur.getString(2) != null)
                    out.cid = cur.getString(2);
                if (cur.getString(3) != null)
                    out.name = cur.getString(3);
                if (cur.getString(4) != null)
                    out.dtview = cur.getString(4);
                if (cur.getString(5) != null)
                    out.notif = cur.getString(5);
                if (cinfo) {
                    modContact c = RCContacts.getContactByNumber(ctx, out.phone);
                    if (c != null) {
                        out.name = c.name;
                        out.icon = c.icon;
                    }
                }
                return out;
            } else {
                return null;
            }

        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_list).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // GET ALL NOT VIEWED NOTIFICATIONS
    //----------------------------------------------------------------------------------------------

    public modContact[] listNVNotif() throws RCDBException {
        SQLiteDatabase db = this.dbh.getReadableDatabase();
        dbContacts dbC = new dbContacts(ctx);

        String q = "SELECT * FROM blocked_log WHERE notif IS NULL OR notif <> 'X' ";

        try {
            Cursor cur = db.rawQuery(q, null);
            if (cur != null && cur.moveToFirst()) {
                modContact[] outlist = new modContact[cur.getCount()];
                for (int i = 0; i < cur.getCount(); i++) {
                    outlist[i] = new modContact();
                    if (cur.getString(0) != null)
                        outlist[i].phone = cur.getString(0);
                    if (cur.getString(1) != null)
                        outlist[i].dtcall = cur.getString(1);
                    if (cur.getString(2) != null)
                        outlist[i].cid = cur.getString(2);
                    if (cur.getString(3) != null)
                        outlist[i].dtview = cur.getString(3);
                    if (cur.getString(4) != null)
                        outlist[i].notif = cur.getString(4);

                    modContact c = RCContacts.getContactByNumber(ctx, outlist[i].phone);
                    if (c != null) {
                        outlist[i].cid = c.cid;
                        outlist[i].name = c.name;
                        outlist[i].icon = c.icon;
                    }
                    cur.moveToNext();
                }
                return outlist;
            } else {
                return null;
            }

        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_list).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }

    //----------------------------------------------------------------------------------------------
    // CLEAR SPECIFIC NUMBER BLOCK LOGS
    //----------------------------------------------------------------------------------------------

    public boolean clearLogs(String number) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbblock_msg_e_informnumber).toString());

        SQLiteDatabase db = this.dbh.getWritableDatabase();

        number = number.replaceAll("[^0-9]", "");
        number = number.replaceAll(" ", "");

        try {
            if (db.delete("blocked_log", "number = ?", new String[]{number}) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_delete).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_delete).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // CLEAR ALL BLOCK LOGS
    //----------------------------------------------------------------------------------------------

    public boolean clearAll() throws RCDBException {

        SQLiteDatabase db = this.dbh.getWritableDatabase();

        try {
            if (db.delete("blocked_log", null, null) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_delete).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_delete).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }
}
