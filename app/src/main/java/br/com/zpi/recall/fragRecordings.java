package br.com.zpi.recall;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.Metadata;
import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import br.com.zpi.recall.db.dbContacts;
import br.com.zpi.recall.db.dbRecord;
import br.com.zpi.recall.extendables.MyBaseListLineAdapter;
import br.com.zpi.recall.extendables.MyTask;
import br.com.zpi.recall.model.modContact;
import br.com.zpi.recall.model.modRecord;
import br.com.zpi.recall.tools.Convert;
import br.com.zpi.recall.tools.RCContacts;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

public class fragRecordings extends Fragment {
    private ListView lstRec = null;
    private Context myContext = null;

    private RelativeLayout mainLayout = null;
    private actContacts mainActivity = null;
    private SwipeRefreshLayout swipeRec = null;
    private TextView txtNoRes = null;
    private modRecord[] records = null;
    private Spinner spinRec = null;
    private ArrayList<String> delfiles = null;
    private MyTask uploadTask = null;
    private modRecord[] gdrive_records = null;

    public fragRecordings() {
        // Required empty public constructor
    }


    //##############################################################################################
    // CREATE VIEW
    //##############################################################################################

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_recordings, container, false);
        myContext = getActivity().getBaseContext();
        mainActivity = (actContacts) getActivity();
        return mainLayout;
    }


    //##############################################################################################
    // VIEW CREATED
    //##############################################################################################

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lstRec = (ListView) mainLayout.findViewById(R.id.lstRec);
        swipeRec = (SwipeRefreshLayout) mainLayout.findViewById(R.id.swipeRec);
        txtNoRes = (TextView) mainLayout.findViewById(R.id.txtNoRes);
        spinRec = (Spinner) mainLayout.findViewById(R.id.spinRec);
        ImageView imgDelR = (ImageView) mainLayout.findViewById(R.id.imgDelR);
        ImageView imgGDSD = (ImageView) mainLayout.findViewById(R.id.imgGDSD);
        ImageView imgGDSU = (ImageView) mainLayout.findViewById(R.id.imgGDSU);

        delfiles = new ArrayList<String>();
        //Load Spinner
        ArrayAdapter<CharSequence> al = ArrayAdapter.createFromResource(getActivity(),
                R.array.arr_record, R.layout.spin_basic);
        al.setDropDownViewResource(R.layout.spin_basic);

        spinRec.setAdapter(al);
        spinRec.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loadRec();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        // DELETE CLICK LISTENER
        imgDelR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(delfiles.size() > 0){
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getActivity().getResources().getText(R.string.record_confirm_delfiles).toString())
                            .setMessage(getActivity().getResources().getText(R.string.record_confirm_delfiles_info).toString())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    if (whichButton == Dialog.BUTTON_POSITIVE) {
                                        dbRecord dbR = new dbRecord(mainActivity);
                                        for(String s : delfiles)
                                            dbR.delRecord(s);
                                    }
                                    loadRec();
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();

                }else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(getActivity().getResources().getText(R.string.record_confirm_clean).toString())
                            .setMessage(getActivity().getResources().getText(R.string.record_confirm_clean_info).toString())
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    if (whichButton == Dialog.BUTTON_POSITIVE) {
                                        dbRecord dbR = new dbRecord(mainActivity);
                                        if (dbR.clearAll()) {
                                            loadRec();
                                        } else {

                                        }

                                    }
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                }

            }
        });


        // GDRIVE SYNC UP
        imgGDSU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(records != null && records.length > 0){
                if(uploadTask == null || (uploadTask.getStatus() != AsyncTask.Status.RUNNING))
                    uploadTask = new MyTask() {
                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                        }

                        @Override
                        protected Object doInBackground(Object[] objects) {
                            if(delfiles != null && delfiles.size() > 0){
                                for(int c = 0; c < delfiles.size(); c++)
                                    mainActivity.gdriveUpload(delfiles.get(c));
                            }else{
                                for (modRecord r : records){
                                    mainActivity.gdriveUpload(r.filepath);
                                }
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Object o) {
                            super.onPostExecute(o);
                        }
                    };
                    uploadTask.execute();
            }else{
                mainActivity.mdiag.displayMessage(getResources().getText(R.string.rcfiles_norecs).toString(), 2);
            }

            }
        });


        if (mainActivity.prefs.getString("gdrive", "").trim().toUpperCase().equals("X")) {
            imgGDSU.setVisibility(View.VISIBLE);
            imgGDSD.setVisibility(View.VISIBLE);
            loadGdriveFiles();
        }else{
            imgGDSU.setVisibility(View.GONE);
            imgGDSD.setVisibility(View.GONE);
            loadRec();
        }


        swipeRec.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mainActivity.prefs.getString("gdrive", "").trim().toUpperCase().equals("X")) {
                    loadGdriveFiles();
                }else{
                    loadRec();
                }

            }
        });

    }


    //##############################################################################################
    //##############################################################################################
    // CLASS FUNCTIONS
    //##############################################################################################
    //##############################################################################################


    //----------------------------------------------------------------------------------------------
    // LOAD RECORDS
    //----------------------------------------------------------------------------------------------

    public void loadRec() {
        txtNoRes.setVisibility(View.GONE);
        dbRecord dbR = new dbRecord(mainActivity);
        String ord = null;
        TextView txtSpace = (TextView) mainLayout.findViewById(R.id.txtSpace);
        long usedspace = 0;

        lstRec.setAdapter(null);
        lstRec.bringToFront();

        if (spinRec.getSelectedItemPosition() > 0)
            ord = "ASC";
        else
            ord = "DESC";

        try {
            records = dbR.getRecords(ord);

            if (records != null && records.length > 0) {
                ArrayList<modRecord> a = new ArrayList<modRecord>();

                for (modRecord r : records) {
                    if (r != null) {
                        if(r.fileSize != null && r.fileSize.trim().length() > 0)
                            usedspace += Long.valueOf(r.fileSize);
                        a.add(r);
                    }
                }
                MyBaseListLineAdapter adFiles = new MyBaseListLineAdapter(mainActivity, a) {
                    @Override
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        modRecord r = (modRecord) getContent().get(i);
                        modContact c = RCContacts.getContactByNumber(mainActivity, r.number);
                        String name = r.number;
                        String phone = r.number;
                        String date = r.dtrec + " " + r.timerec;
                        Bitmap img = null;
                        if (c != null) {
                            name = c.name;
                            if (c.icon != null)
                                img = c.icon;
                        }
                        boolean indrive = false;
                        if(gdrive_records != null){
                            for (modRecord rec : gdrive_records)
                                if(rec.filepath.trim().equals(r.filepath.trim())){
                                    indrive = true;
                                    break;
                                }
                        }
                        return drawFiles(name, phone, date, r.filepath, img, r.fileSize, indrive);
                    }
                };
                lstRec.setAdapter(adFiles);
            } else
                txtNoRes.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            txtNoRes.setVisibility(View.VISIBLE);
        }

        long remainingspace = 0;
        txtSpace.setVisibility(View.GONE);
        try {
            File recDir = new File(Environment.getExternalStorageDirectory(), RCConstants.recpath);
            if(recDir != null) {
                remainingspace = recDir.getFreeSpace();
                txtSpace.setVisibility(View.VISIBLE);
                txtSpace.setText("Utilizado: " + Convert.formatSize(usedspace) + "\nLivre: " + Convert.formatSize(remainingspace));
            }
        }catch (Exception e){

        }

        swipeRec.setRefreshing(false);
    }

    //----------------------------------------------------------------------------------------------
    // LOAD GDRIVE FILES
    //----------------------------------------------------------------------------------------------
    public void loadGdriveFiles(){
        Drive.DriveApi.requestSync(mainActivity.mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                    mainActivity.gdriveRecAll.listChildren(mainActivity.mGoogleApiClient).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
                        @Override
                        public void onResult(@NonNull DriveApi.MetadataBufferResult metadataBufferResult) {
                            if (metadataBufferResult != null && metadataBufferResult.getMetadataBuffer() != null && metadataBufferResult.getMetadataBuffer().getCount() > 0) {
                                gdrive_records = new modRecord[metadataBufferResult.getMetadataBuffer().getCount()];
                                int cn = 0;
                                for (Metadata m : metadataBufferResult.getMetadataBuffer()) {
                                    gdrive_records[cn] = new modRecord();
                                    gdrive_records[cn].filepath = m.getOriginalFilename();
                                    gdrive_records[cn].fileSize = String.valueOf(m.getFileSize());
                                    gdrive_records[cn].dtrec = ZonedDateTime.parse(m.getCreatedDate().toString()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                                    cn++;
                                }
                            } else {
                                gdrive_records = null;
                            }
                            loadRec();
                        }
                    });
            }
        });


    }


    //----------------------------------------------------------------------------------------------
    // DRAW LINE
    //----------------------------------------------------------------------------------------------

    public RelativeLayout drawFiles(String name, String phone, String date, String filepath, Bitmap icon, String fsize, boolean indrive) {
        dbContacts dbC = new dbContacts(getActivity());

        LayoutInflater inf = getActivity().getLayoutInflater();
        RelativeLayout lay = (RelativeLayout) inf.inflate(R.layout.line_rcfiles, null);

        TextView txtName = (TextView) lay.findViewById(R.id.txtName);
        TextView txtPhone = (TextView) lay.findViewById(R.id.txtPhone);
        TextView txtDate = (TextView) lay.findViewById(R.id.txtDate);
        ImageView imgProfPic = (ImageView) lay.findViewById(R.id.imgProfPic);
        ImageView imgPlay = (ImageView) lay.findViewById(R.id.imgPlay);
        TextView txtFSize = (TextView) lay.findViewById(R.id.txtFSize);
        CheckBox ckLine = (CheckBox) lay.findViewById(R.id.ckLine);
        ImageView imgGdriveRec = (ImageView) lay.findViewById(R.id.imgGdriveRec);
        ckLine.setTag(filepath);
        txtName.setText(name);
        txtPhone.setText(RCContacts.formatNumber(phone));
        txtDate.setText(date);

        if (mainActivity.prefs.getString("gdrive", "").trim().toUpperCase().equals("X")) {
            imgGdriveRec.setVisibility(View.VISIBLE);
            if (indrive)
                imgGdriveRec.setAlpha(1f);
        }else{
            imgGdriveRec.setVisibility(View.GONE);
        }
        //ADD TO DEL
        ckLine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    delfiles.add(compoundButton.getTag().toString());
                }else{
                    delfiles.remove(compoundButton.getTag().toString());
                }
            }
        });


        // FILE SIZE
        if(fsize != null && fsize.trim().length() > 0)
            txtFSize.setText(Convert.formatSize(Long.valueOf(fsize)));
        else
            txtFSize.setVisibility(View.GONE);

        if (icon != null)
            imgProfPic.setImageBitmap(icon);

        //IMAGE PLAY
        if (filepath != null && filepath.trim().length() > 0) {
            imgPlay.setTag(filepath);
            imgPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setAction(android.content.Intent.ACTION_VIEW);
                    File sampleDir = new File(Environment.getExternalStorageDirectory(), RCConstants.recpath);
                    File audiofile = new File(sampleDir, view.getTag().toString());
                    if (audiofile.exists() && audiofile.isFile() && audiofile.length() > 0 && audiofile.canRead()) {
                        intent.setDataAndType(Uri.fromFile(audiofile), "audio/*");
                        startActivity(intent);
                    } else {
                        view.setVisibility(View.GONE);
                    }
                }
            });
        } else
            imgPlay.setVisibility(View.GONE);
        return lay;
    }


}
