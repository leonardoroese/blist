package br.com.zpi.recall;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import br.com.zpi.recall.db.RCDBException;
import br.com.zpi.recall.db.dbBlock;
import br.com.zpi.recall.db.dbContacts;
import br.com.zpi.recall.db.dbRecord;
import br.com.zpi.recall.extendables.MyBaseListLineAdapter;
import br.com.zpi.recall.extendables.MyTask;
import br.com.zpi.recall.model.modContact;
import br.com.zpi.recall.model.modRecord;
import br.com.zpi.recall.tools.Generic;
import br.com.zpi.recall.tools.RCContacts;


/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
public class fragContacts extends Fragment {
    private ListView lstContacts = null;
    private Context myContext = null;
    private modContact[] contacts = null;
    private RelativeLayout mainLayout = null;
    private actContacts mainActivity = null;
    private TextView txtNoRes = null;
    private static int RESULT_LIMIT = 20;
    private static int resPage = 0;
    private boolean resEnd = false;
    private EditText inpSearch = null;
    private static final int CONTACTS_RES_REFRESH = 44;
    private static final int CONTACTS_REQ_DISPLAY = 45;
    private int lstindex = 0;
    private MyTask task = null;
    private SwipeRefreshLayout swipeContact = null;

    public fragContacts() {
        // Required empty public constructor
    }


    //##############################################################################################
    // CREATE VIEW
    //##############################################################################################

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_contacts, container, false);
        mainActivity = (actContacts) getActivity();
        return mainLayout;
    }


    //##############################################################################################
    // VIEW CREATED
    //##############################################################################################

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myContext = getActivity().getBaseContext();
        swipeContact = (SwipeRefreshLayout) mainLayout.findViewById(R.id.swipeContact);
        lstContacts = (ListView) mainLayout.findViewById(R.id.lstContacts);
        txtNoRes = (TextView) mainLayout.findViewById(R.id.txtNoRes);
        inpSearch = (EditText) mainLayout.findViewById(R.id.inpSearch);

        swipeContact.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadContacts(true);
            }
        });

        // TEXT FILTER CHANGE LISTENER
        inpSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                loadContacts(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        loadContacts(true);

    }


    //##############################################################################################
    // VIEW CREATED
    //##############################################################################################

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CONTACTS_RES_REFRESH)
            loadContacts(true);
    }

    //##############################################################################################
    // PAUSE
    //##############################################################################################

    @Override
    public void onPause() {
        super.onPause();
        if(lstContacts != null)
            lstindex = lstContacts.getFirstVisiblePosition();
    }

    //##############################################################################################
    // RESUME
    //##############################################################################################

    @Override
    public void onResume() {
        super.onResume();
        if(lstContacts != null)
            lstContacts.setSelectionFromTop(lstindex, 0);
    }


    //##############################################################################################
    //##############################################################################################
    // CLASS FUNCTIONS
    //##############################################################################################
    //##############################################################################################

    //----------------------------------------------------------------------------------------------
    // LOAD CONTACTS
    //----------------------------------------------------------------------------------------------

    public void loadContacts(boolean resetpaging) {

        if(task != null && task.getStatus() == AsyncTask.Status.RUNNING){
            if(!resetpaging)
                return;
            else
                task.cancel(true);
        }

        if (resetpaging) {
            resPage = 0;
            lstContacts.setAdapter(null);
            lstContacts.bringToFront();
            resEnd = false;
        }
        if (resEnd) {
            swipeContact.setRefreshing(false);
            return;
        }
        txtNoRes.setVisibility(View.GONE);

        task = new MyTask() {
            private String filter = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (inpSearch.getText().toString().trim().length() > 0)
                    filter = inpSearch.getText().toString();
                swipeContact.setRefreshing(true);
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    modContact[] c = RCContacts.getContactList(getActivity(), null,filter, null, RESULT_LIMIT, resPage);
                    return c;
                } catch (Exception e) {

                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                RCReceiver.displayNotifB(getActivity());
                if(o != null){
                    modContact[] cts = (modContact[]) o;
                    ArrayList<modContact> a = new ArrayList<modContact>();
                    if (resPage == 0) {
                        for (modContact c : cts)
                            if (c.phone != null && c.phone.trim().length() > 0)
                                a.add(c);

                        MyBaseListLineAdapter adFiles = new MyBaseListLineAdapter(getActivity(), a) {
                            @Override
                            public View getView(int i, View view, ViewGroup viewGroup) {
                                modContact contact = (modContact) getContent().get(i);
                                if (!resEnd && i > 0 && i >= (getContent().size() - 3) && getContent().size() >= RESULT_LIMIT) {
                                    resPage = i;
                                    loadContacts(false);
                                }
                                return drawContact(contact, i);
                            }
                        };
                        lstContacts.setAdapter(adFiles);
                    } else {
                        MyBaseListLineAdapter ad = (MyBaseListLineAdapter) lstContacts.getAdapter();
                        a = ad.getContent();
                        for (modContact c : cts)
                            if (c.phone != null && c.phone.trim().length() > 0)
                                a.add(c);
                        ad.setContent(a);
                        ad.notifyDataSetChanged();
                    }
                }else{
                    if (resPage == 0)
                        txtNoRes.setVisibility(View.VISIBLE);
                    resEnd = true;
                }
                swipeContact.setRefreshing(false);
            }
        };

        task.execute();

    }


    //----------------------------------------------------------------------------------------------
    // DRAW LINE
    //----------------------------------------------------------------------------------------------


    public RelativeLayout drawContact(modContact contact, int index) {
        LayoutInflater inf = getActivity().getLayoutInflater();
        RelativeLayout lay = (RelativeLayout) inf.inflate(R.layout.line_contact, null);
        dbContacts dbC = new dbContacts(getActivity());
        dbBlock dbB = new dbBlock(getActivity());
        dbRecord dbR = new dbRecord(getActivity());

        TextView txtName = (TextView) lay.findViewById(R.id.txtName);
        TextView txtPhone = (TextView) lay.findViewById(R.id.txtPhone);
        ImageView imgBlock = (ImageView) lay.findViewById(R.id.imgBlock);
        ImageView imgRec = (ImageView) lay.findViewById(R.id.imgRec);
        ImageView imgProfPic = (ImageView) lay.findViewById(R.id.imgProfPic);
        TextView txtBlocked = (TextView) lay.findViewById(R.id.txtBlocked);
        TextView txtReced = (TextView) lay.findViewById(R.id.txtReced);

        RelativeLayout layIBlock = (RelativeLayout) lay.findViewById(R.id.layIBlock);
        RelativeLayout layIRec = (RelativeLayout) lay.findViewById(R.id.layIRec);

        lay.setTag("user" + contact.cid);
        lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = view.getTag().toString().substring(4);
                if (id != null && id.trim().length() > 0) {
                    Intent i = new Intent(getActivity(), actUser.class);
                    i.putExtra("user_id", id);
                    startActivityForResult(i, CONTACTS_REQ_DISPLAY);
                }
            }
        });


        if (contact.name != null && contact.name.trim().length() > 0)
            txtName.setText(contact.name);
        else if (contact.phone != null && contact.phone.trim().length() > 0)
            txtName.setText(contact.phone);
        else
            txtName.setText("--");

        if (contact.phone != null && contact.phone.trim().length() > 0) {
            txtPhone.setText(RCContacts.formatNumber(contact.phone));

        } else
            txtPhone.setText("--");

        try {
            modContact c = dbC.getBlocked(contact.phone);
            if (c != null)
                if(c != null && c.wildcard != null && c.wildcard.trim().equals("X"))
                    imgBlock.setImageDrawable(getResources().getDrawable(R.drawable.ico_shield_selwild));
                else
                    imgBlock.setImageDrawable(getResources().getDrawable(R.drawable.ico_shield_sel));
            else
                imgBlock.setImageDrawable(getResources().getDrawable(R.drawable.ico_shieldblue));
        } catch (RCDBException e) {
            imgBlock.setImageDrawable(getResources().getDrawable(R.drawable.ico_shieldblue));
        }

        try {
            modContact c = dbC.getRec(contact.phone);
            if (c != null)
                imgRec.setImageDrawable(getResources().getDrawable(R.drawable.ico_mic_sel));
            else
                imgRec.setImageDrawable(getResources().getDrawable(R.drawable.ico_micblue));
        } catch (RCDBException e) {
            imgRec.setImageDrawable(getResources().getDrawable(R.drawable.ico_micblue));
        }

        if (contact.icon != null)
            imgProfPic.setImageBitmap(contact.icon);

        layIBlock.setTag("blnum" + String.valueOf(index));
        imgBlock.setTag("blnum" + contact.phone);
        layIBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idx = Integer.parseInt(view.getTag().toString().substring(5));
                modContact c = (modContact) ((MyBaseListLineAdapter)lstContacts.getAdapter()).getContent().get(idx);
                ImageView im = (ImageView) view.findViewWithTag("blnum"+c.phone);
                dbContacts dbC = new dbContacts(myContext);
                String bl = c.phone;
                if(c.name != null && c.name.trim().length() > 0)
                    bl = c.name;
                if (im.getDrawable().getConstantState().equals
                        (getResources().getDrawable(R.drawable.ico_shieldblue).getConstantState())) {
                    try {
                        dbC.blockNum(c.phone, null);
                        Generic.replaceAllIMVDrawables(mainLayout, im.getTag().toString(), getResources().getDrawable(R.drawable.ico_shield_sel));
                        mainActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_block_on).toString() + " " + bl, 2);
                    } catch (RCDBException e) {

                    }
                } else {
                    try {
                        dbC.releaseNum(c.phone);
                        Generic.replaceAllIMVDrawables(mainLayout, im.getTag().toString(), getResources().getDrawable(R.drawable.ico_shieldblue));
                        mainActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_block_off).toString() + " " + bl, 2);
                    } catch (RCDBException e) {

                    }
                }
                im.refreshDrawableState();
            }
        });

        layIRec.setTag("recnum" + String.valueOf(index));
        imgRec.setTag("recnum" + contact.phone);
        layIRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idx = Integer.parseInt(view.getTag().toString().substring(6));
                modContact c = (modContact) ((MyBaseListLineAdapter)lstContacts.getAdapter()).getContent().get(idx);
                ImageView im = (ImageView) view.findViewWithTag("recnum"+c.phone);
                dbContacts dbC = new dbContacts(myContext);
                String bl = c.phone;
                if(c.name != null && c.name.trim().length() > 0)
                    bl = c.name;
                if (im.getDrawable().getConstantState().equals
                        (getResources().getDrawable(R.drawable.ico_micblue).getConstantState())) {
                    try {
                        dbC.recNum(c.phone, null);
                        Generic.replaceAllIMVDrawables(mainLayout, im.getTag().toString(), getResources().getDrawable(R.drawable.ico_mic_sel));
                        mainActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_rec_on).toString() + " " + bl, 2);
                    } catch (RCDBException e) {

                    }
                } else {
                    try {
                        dbC.norecNum(c.phone);
                        Generic.replaceAllIMVDrawables(mainLayout, im.getTag().toString(), getResources().getDrawable(R.drawable.ico_micblue));
                        mainActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_rec_off).toString() + " " + bl, 2);
                    } catch (RCDBException e) {

                    }
                }
                im.refreshDrawableState();
            }
        });

        // Get Blocked
        try {
            modContact[] logs = dbB.listLogs(contact.phone);
            if (logs != null && logs.length > 0) {
                txtBlocked.setText(String.valueOf(logs.length));
            }


        } catch (RCDBException e) {

        }

        //Get recordings
        modRecord[] recs = dbR.getNumRecords(contact.phone);
        if (recs != null && recs.length > 0) {
            txtReced.setText(String.valueOf(recs.length));
        }

        return lay;
    }



}
