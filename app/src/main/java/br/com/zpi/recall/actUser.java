package br.com.zpi.recall;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.zpi.recall.db.RCDBException;
import br.com.zpi.recall.db.dbBlock;
import br.com.zpi.recall.db.dbContacts;
import br.com.zpi.recall.db.dbRecord;
import br.com.zpi.recall.extendables.MyActivity;
import br.com.zpi.recall.model.modContact;
import br.com.zpi.recall.model.modRecord;
import br.com.zpi.recall.tools.RCContacts;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

public class actUser extends MyActivity {
    private static String user_id = null;
    private static String user_name = null;
    private static String user_phone = null;
    private static modContact contact = null;
    private static final int CONTACTS_RES_REFRESH = 44;
    private static boolean refresh = false;
    private static TextView txtUserName = null;
    private static TextView txtUserPhone = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_user);
        dbBlock dbB = new dbBlock(this);
        dbRecord dbR = new dbRecord(this);

        ImageView imgUser = (ImageView) findViewById(R.id.imgUser);
        txtUserName = (TextView) findViewById(R.id.txtUserName);
        txtUserPhone = (TextView) findViewById(R.id.txtUserPhone);
        TextView txtUBlocked = (TextView) findViewById(R.id.txtUBlocked);
        TextView txtURecorded = (TextView) findViewById(R.id.txtURecorded);
        Button btnUBlocked = (Button) findViewById(R.id.btnUBlocked);
        Button btnBack = (Button) findViewById(R.id.btnBack);
        ImageButton imgCall = (ImageButton) findViewById(R.id.imgCall);
        dbContacts dbC = new dbContacts(getBaseContext());
        CheckBox ckUBlock = (CheckBox) findViewById(R.id.ckUBlock);
        CheckBox ckURecorded = (CheckBox) findViewById(R.id.ckURecorded);

        if (getIntent().hasExtra("user_id"))
            user_id = getIntent().getStringExtra("user_id");
        if (getIntent().hasExtra("user_name"))
            user_name = getIntent().getStringExtra("user_name");
        if (getIntent().hasExtra("user_phone"))
            user_phone = getIntent().getStringExtra("user_phone");

        modContact[] contacts = null;

        if(user_id != null && user_id.trim().length() > 0)
            contacts = RCContacts.getContactList(this, user_id, null, null, 10, 0);

        if (contacts != null && contacts.length > 0) {
            contact = contacts[0];

            if (contact.name == null || contact.name.trim().length() <= 0)
                txtUserName.setText(RCContacts.formatNumber(contact.phone));
            else
                txtUserName.setText(contact.name);

            if (contact.phone != null && contact.phone.trim().length() > 0) {
                imgCall.setTag(contact.phone);
                txtUserPhone.setText(RCContacts.formatNumber(contact.phone));
            }

            if (contact.icon != null)
                imgUser.setImageBitmap(contact.icon);

        } else {

            if (user_name == null || user_name.trim().length() <= 0)
                if (user_phone != null)
                    txtUserName.setText(user_phone);
                else
                    txtUserName.setText(user_name);

            if (user_phone != null && user_phone.trim().length() > 0) {
                txtUserPhone.setText(user_phone);
                imgCall.setTag(user_phone);
            }

        }

        // Blocked Count
        try {
            modContact[] logs = dbB.listLogs(txtUserPhone.getText().toString());
            if (logs != null && logs.length > 0) {
                txtUBlocked.setText(String.valueOf(logs.length));
            }
        } catch (RCDBException e) {

        }

        // Record Count
        modRecord[] recs = dbR.getNumRecords(txtUserPhone.getText().toString());
        if (recs != null && recs.length > 0) {
            txtURecorded.setText(String.valueOf(recs.length));
        }

        // FILL Checkbox Block STATUS
        try {
            modContact c = dbC.getBlocked(txtUserPhone.getText().toString());
            if(c != null && c.wildcard != null && c.wildcard.trim().equals("X"))
                ckUBlock.setVisibility(View.GONE);
            else if (c != null)
                ckUBlock.setChecked(true);
            else
                ckUBlock.setChecked(false);
        } catch (RCDBException e) {
            ckUBlock.setChecked(false);
        }

        // FILL Checkbox Rec STATUS
        try {
            modContact c = dbC.getRec(txtUserPhone.getText().toString());
            if (c != null)
                ckURecorded.setChecked(true);
            else
                ckURecorded.setChecked(false);
        } catch (RCDBException e) {
            ckURecorded.setChecked(false);
        }

        // LISTENER - Checkbox BLOCK
        ckUBlock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dbContacts dbC = new dbContacts(getBaseContext());
                if (b) {
                    try {
                        dbC.blockNum(txtUserPhone.getText().toString(), null);
                        mdiag.displayMessage(getResources().getText(R.string.msg_block_on).toString() + " " + txtUserName.getText().toString(), 2);
                    } catch (RCDBException e) {

                    }
                } else {
                    try {
                        dbC.releaseNum(txtUserPhone.getText().toString());
                        mdiag.displayMessage(getResources().getText(R.string.msg_block_off).toString() + " " + txtUserName.getText().toString(), 2);
                    } catch (RCDBException e) {

                    }
                }
                refresh = true;
            }
        });

        // LISTENER - Checkbox REC
        ckURecorded.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dbContacts dbC = new dbContacts(getBaseContext());
                if (b) {
                    try {
                        dbC.recNum(txtUserPhone.getText().toString(), null);
                        mdiag.displayMessage(getResources().getText(R.string.msg_rec_on).toString() + " " + txtUserName.getText().toString(), 2);
                    } catch (RCDBException e) {

                    }
                } else {
                    try {
                        dbC.norecNum(txtUserPhone.getText().toString());
                        mdiag.displayMessage(getResources().getText(R.string.msg_rec_off).toString() + " " + txtUserName.getText().toString(), 2);
                    } catch (RCDBException e) {

                    }
                }
                refresh = true;
            }
        });

        // LISTENER - CALL NUMBER
        imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("tel:" + view.getTag().toString());
                Intent intent = new Intent(Intent.ACTION_DIAL, uri);
                startActivity(intent);
            }
        });


        btnUBlocked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(actUser.this, actLogUser.class);
                i.putExtra("user_id", user_id);
                startActivity(i);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (refresh)
            setResult(CONTACTS_RES_REFRESH);
        finish();
    }
}