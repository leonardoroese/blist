package br.com.zpi.recall.extendables;

import android.os.AsyncTask;

	/*
	 * Copyright 2018 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

public abstract class MyTask extends AsyncTask {

    public Object param = null;
    private taskfinished myListener = null;

    public Object getParam() {
        return param;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (myListener != null)
            myListener.onFinishedExec();
        cancel(true);
    }

    public void setParam(String param) {
        this.param = param;
    }

    public void setMyListener(taskfinished myListener) {
        this.myListener = myListener;
    }

    public taskfinished getMyListener() {
        return myListener;
    }

    public interface taskfinished {
        void onFinishedExec();
    }


}
