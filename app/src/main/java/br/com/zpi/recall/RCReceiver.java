package br.com.zpi.recall;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.widget.RemoteViews;

import com.android.internal.telephony.ITelephony;
import java.lang.reflect.Method;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import br.com.zpi.recall.db.RCDBException;
import br.com.zpi.recall.db.dbBlock;
import br.com.zpi.recall.db.dbContacts;
import br.com.zpi.recall.model.modContact;
import br.com.zpi.recall.tools.RCContacts;

	/*
	 * Copyright 2018 Leonardo Germano Roese
	 *
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 *
	 * http://www.apache.org/licenses/LICENSE-2.0
	 *
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

public class RCReceiver extends BroadcastReceiver {

    private static RCMediaRecorder rec = null;
    private String action = null;
    private static TelephonyManager tmgr = null;
    private static ITelephony telephonyService = null;
    private Context ctx = null;
    private SharedPreferences prefs = null;
    private static boolean recall = false;
    private static boolean blockall = false;
    private static final int RECALL_NOTIF_BLOCKED = 667;
    private static Notification notificationB = null;
    private static int replyMsg = 0;
    private static String customReply = null;
    private static boolean replysms = false;


    public RCReceiver() {
        super();
        this.rec = new RCMediaRecorder();
    }


    public RCReceiver(Context context, RCMediaRecorder rec) {
        super();
        this.rec = rec;
        this.ctx = context;
    }

    //##############################################################################################
    // PEEK
    //##############################################################################################

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }

    //##############################################################################################
    // RECEIVE
    //##############################################################################################

    @Override
    public void onReceive(Context context, Intent intent) {
        ctx = context;
        action = intent.getAction();
        prefs = context.getSharedPreferences("recall",
                Context.MODE_PRIVATE);
        recall = false;
        blockall = false;

        replyMsg = prefs.getInt("replyMsg",0);
        customReply = prefs.getString("customReply","");
        if(prefs.getString("reply", "").trim().equals("X"))
            replysms = true;

        if (prefs != null) {
            if (prefs.getString("blockall", "").trim().toUpperCase().equals("X"))
                blockall = true;
            if (prefs.getString("recall", "").trim().toUpperCase().equals("X"))
                recall = true;

        }

        tmgr = (TelephonyManager) ctx
                .getSystemService(Context.TELEPHONY_SERVICE);
        try {

            Class cl = Class.forName(tmgr.getClass().getName());
            Method m = cl.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(tmgr);
        } catch (Exception e) {

        }
        MyPhoneStateListener PhoneListener = new MyPhoneStateListener(rec);
        tmgr.listen(PhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------
    // INTERNAL FUNCTIONS
    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    public static void displayNotifB(Context ctx) {
        if(ctx == null)
            return;
        SharedPreferences prefs = ctx.getSharedPreferences("recall",
                Context.MODE_PRIVATE);

        if (!prefs.getString("blockall", "").trim().toUpperCase().equals("X"))
            return;

        NotificationManager bNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        RemoteViews contentViewB = new RemoteViews(ctx.getPackageName(), R.layout.notif_block);

        dbBlock dbB = new dbBlock(ctx);
        modContact[] notifs = null;
        try {
            notifs = dbB.listNVNotif();
        } catch (RCDBException e) {

        }
        CharSequence tickerText = "BLIST BNOTIF";
        Notification.Builder builder = new Notification.Builder(ctx);

        if (notifs != null && notifs.length > 0) {
            notificationB = builder.getNotification();
            notificationB.tickerText = tickerText;
            notificationB.icon = R.drawable.blisticon_t;

            // Initialize notifier
            if (notifs.length > 1) {
                contentViewB.setTextViewText(R.id.txtNBlockedID, String.valueOf(notifs.length) + " " + ctx.getResources().getText(R.string.notif_block_numinfo));
            } else {
                if(notifs[0].name == null || notifs[0].name.trim().length() < 3)
                    notifs[0].name = notifs[0].phone;
                contentViewB.setTextViewText(R.id.txtNBlockedID, notifs[0].name);
            }

            notificationB.contentView = contentViewB;
            notificationB.flags = Notification.FLAG_GROUP_SUMMARY;
            bNotificationManager.notify(RECALL_NOTIF_BLOCKED, notificationB);
        } else {
            if (bNotificationManager != null)
                bNotificationManager.cancel(RECALL_NOTIF_BLOCKED);

        }

    }

    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------
    // INTERNAL CLASSES
    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    class MyPhoneStateListener extends PhoneStateListener {
        TelephonyManager telephony = null;

        private RCMediaRecorder rec = null;

        public MyPhoneStateListener(RCMediaRecorder rec) {
            this.rec = rec;
        }

        public void onCallStateChanged(int state, String incomingNumber) {
            dbContacts dbC = new dbContacts(ctx);
            dbBlock dbB = new dbBlock(ctx);


            if (state == TelephonyManager.CALL_STATE_RINGING) {
                try {
                    modContact b = dbC.getBlocked(incomingNumber);
                    modContact a = dbC.getAllowed(incomingNumber);
                    modContact c = RCContacts.getContactByNumber(ctx,incomingNumber);
                    String cname = null;
                    if(c != null)
                        cname = c.name;
                    if ((b != null || blockall) && a == null) {
                        //setResultData(null);
                        if(b == null && replysms){
                            if(replyMsg >= 6){
                                sendSMS(incomingNumber,customReply);
                            }else{
                                sendSMS(incomingNumber,ctx.getResources().getStringArray(R.array.reply_txt)[replyMsg]);
                            }
                        }

                        try {
                            if (telephonyService != null) {
                                telephonyService.silenceRinger();
                                telephonyService.endCall();
                                dbB.logNum(incomingNumber, null, cname, null);
                                displayNotifB(ctx);
                            }
                        } catch (Exception e) {
                        }
                    } else {

                    }
                } catch (RCDBException e) {

                }


            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                boolean record = false;
                try {
                    modContact c = dbC.getRec(incomingNumber);
                    if (c != null || recall)
                        record = true;
                } catch (Exception e) {

                }
                if (!rec.recording && record)
                    rec.start(incomingNumber + "_" + ZonedDateTime.now().format(DateTimeFormatter.ofPattern(RCConstants.rectimestamp)));
            } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                try {
                    if (rec.recording)
                        rec.stop();
                } catch (Exception e) {

                }
            }
        }
    }

    public void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
        } catch (Exception ex) {

        }
    }
}
