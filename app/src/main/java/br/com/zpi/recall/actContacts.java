package br.com.zpi.recall;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.astuetz.PagerSlidingTabStrip;

import br.com.zpi.recall.extendables.MyActivity;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

public class actContacts extends MyActivity {
    public ViewPager pager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_contacts);

        if (prefs.getString("gdrive", "").trim().toUpperCase().equals("X")) {
            enableGDrive();
            connectGDrive();
        }
        loadTabs();
    }


    //##############################################################################################
    //##############################################################################################
    // CLASS FUNCTIONS
    //##############################################################################################
    //##############################################################################################


    public void loadTabs() {
        pager = (ViewPager) findViewById(R.id.pagerHome);
        PagerSlidingTabStrip pagerstrip = (PagerSlidingTabStrip) findViewById(R.id.pagerHomeStrip);

        pagerstrip.setTextSize(getResources().getDimensionPixelOffset(R.dimen.contacts_pagertab_txt_size));
        pagerstrip.setTypeface(fontRobotoNormal, Typeface.NORMAL);
        pagerstrip.setShouldExpand(true);

        pager.setAdapter(new MyTPageR(getSupportFragmentManager()));
        pagerstrip.setViewPager(pager);

    }


    //##############################################################################################
    //##############################################################################################
    // INNSER CLASSES
    //##############################################################################################
    //##############################################################################################

    class MyTPageR extends FragmentStatePagerAdapter implements PagerSlidingTabStrip.IconTabProvider {
        private int icons[] = {R.drawable.ico_conftab, R.drawable.ico_calltab, R.drawable.ico_shieldtab, R.drawable.ico_mictab, R.drawable.ico_usertab};

        public MyTPageR(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return new fragHome();
                case 1:
                    return new fragRecent();
                case 2:
                    return new fragBlocked();
                case 3:
                    return new fragRecordings();
                case 4:
                    return new fragContacts();
                default:
                    return new fragRecent();
            }
        }

        @Override
        public int getCount() {
            return 5;
        }


        @Override
        public CharSequence getPageTitle(int position) {

            return null;

        }

        @Override
        public int getPageIconResId(int position) {
            return icons[position];
        }
    }

}