package br.com.zpi.recall.db;

import android.content.Context;
import android.os.Environment;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.LastModifiedFileComparator;

import java.io.File;
import java.io.FilenameFilter;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import br.com.zpi.recall.RCConstants;
import br.com.zpi.recall.model.modRecord;

	/*
	 * Copyright 2018 Leonardo Germano Roese
	 *
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 *
	 * http://www.apache.org/licenses/LICENSE-2.0
	 *
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

public class dbRecord {

    private Context ctx = null;
    private DBHELPER dbh = null;

    public dbRecord(Context ctx) {
        this.ctx = ctx;
        this.dbh = new DBHELPER(this.ctx);
    }


    //----------------------------------------------------------------------------------------------
    // GET ALL RECORDS
    //----------------------------------------------------------------------------------------------

    public modRecord[] getRecords(String ord) {
        modRecord[] out = null;

        try {
            File recDir = new File(Environment.getExternalStorageDirectory(), RCConstants.recpath);
            if (recDir.exists()) {
                File[] recs = recDir.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File file, String s) {
                        if (s.indexOf(RCConstants.recext) > (s.length() - 5))
                            return true;
                        return false;
                    }
                });

                if (ord != null && ord.trim().equals("DESC")) {
                    Arrays.sort(recs, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
                } else {
                    Arrays.sort(recs, LastModifiedFileComparator.LASTMODIFIED_COMPARATOR);
                }

                if (recs != null && recs.length > 0) {
                    int cnt = 0;
                    out = new modRecord[recs.length];
                    for (File f : recs) {
                        String filename = f.getName();
                        String[] mtzf = null;
                        if (filename != null && filename.trim().length() > 4 && filename.indexOf("_") > 0) {
                            out[cnt] = new modRecord();
                            mtzf = filename.split("_");
                            if (mtzf != null && mtzf.length > 1) {
                                out[cnt].number = mtzf[0];
                                out[cnt].filepath = filename;
                                out[cnt].fileSize = String.valueOf(f.length());
                                try {
                                    ZonedDateTime dt = ZonedDateTime.parse(mtzf[1]);
                                    out[cnt].dtrec = dt.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                                    out[cnt].timerec = dt.format(DateTimeFormatter.ofPattern("HH:mm"));
                                } catch (Exception e) {

                                }

                            }
                        }
                        cnt++;
                    }
                    return out;
                }
            }
        } catch (Exception e) {

        }

        return null;
    }


    //----------------------------------------------------------------------------------------------
    // GET Number RECORDS
    //----------------------------------------------------------------------------------------------

    public modRecord[] getNumRecords(String number) {
        modRecord[] out = null;
        number = number.replaceAll("[^0-9]", "");
        number = number.replaceAll(" ", "");
        if (number.length() > 9)
            number = number.substring(number.length() - 9);
        try {
            File recDir = new File(Environment.getExternalStorageDirectory(), RCConstants.recpath);
            if (recDir.exists()) {
                File[] recs = recDir.listFiles(new RecFilenameFilter(number));
                if (recs != null && recs.length > 0) {
                    out = new modRecord[recs.length];
                    for (File f : recs) {
                        String filename = f.getName();
                        String[] mtzf = null;
                        int cnt = 0;
                        if (filename != null && filename.trim().length() > 4 && filename.indexOf("_") > 0) {
                            out[cnt] = new modRecord();
                            mtzf = filename.split("_");
                            if (mtzf != null && mtzf.length > 1) {
                                out[cnt].number = mtzf[0];
                                out[cnt].filepath = filename;
                                try {
                                    ZonedDateTime dt = ZonedDateTime.parse(mtzf[1]);
                                    out[cnt].dtrec = dt.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                                    out[cnt].timerec = dt.format(DateTimeFormatter.ofPattern("HH:mm"));
                                } catch (Exception e) {

                                }

                            }
                        }
                        cnt++;
                    }
                    return out;
                }
            }
        } catch (Exception e) {

        }

        return null;
    }


    class RecFilenameFilter implements FilenameFilter {
        private String number = null;

        public RecFilenameFilter(String num) {
            this.number = num;
        }

        @Override
        public boolean accept(File file, String s) {
            if (s.indexOf(RCConstants.recext) > (s.length() - 5) && s.indexOf(number) >= 0)
                return true;
            return false;
        }
    }



    //----------------------------------------------------------------------------------------------
    // DEL RECORD
    //----------------------------------------------------------------------------------------------
    public boolean delRecord(String filepath){
        File recDir = new File(Environment.getExternalStorageDirectory(), RCConstants.recpath + "/" + filepath);
        if (recDir.exists()) {
            try {
                recDir.delete();
                return true;
            }catch (Exception w){

            }
        }
        return false;
    }


    //----------------------------------------------------------------------------------------------
    // CLEAR ALL RECORDS
    //----------------------------------------------------------------------------------------------
    public boolean clearAll(){
        File recDir = new File(Environment.getExternalStorageDirectory(), RCConstants.recpath);
        if (recDir.exists()) {
            try {
                FileUtils.cleanDirectory(recDir);
                return true;
            }catch (Exception w){

            }
        }
        return false;
    }

}
