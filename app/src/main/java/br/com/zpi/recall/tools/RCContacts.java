package br.com.zpi.recall.tools;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.provider.CallLog;
import android.provider.ContactsContract;

import androidx.core.app.ActivityCompat;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import br.com.zpi.recall.model.modContact;

/**
 * Created by leonardo on 30/10/16.
 */

public class RCContacts {

    //----------------------------------------------------------------------------------------------
    // GET CONTACTLIST
    //----------------------------------------------------------------------------------------------

    public static modContact[] getContactList(Context ctx, String contactid, String filter, String order, int limit, int pageIn) {
        modContact[] outlist = null;

        ArrayList<modContact> tmpu = new ArrayList<modContact>();
        ContentResolver cr = ctx.getContentResolver();
        Cursor cursor = null;
        String qorder = null;
        String qwhere = null;
        String[] qwhereargs = null;
        int cntarg = 0;


        qwhere = ContactsContract.Contacts.HAS_PHONE_NUMBER + " > 0";

        if (contactid != null && contactid.trim().length() > 0)
            cntarg++;
        if (filter != null && filter.trim().length() > 0)
            cntarg++;

        if (cntarg == 0)
            qwhereargs = null;
        else
            qwhereargs = new String[cntarg];

        cntarg = 0;

        if (contactid != null && contactid.trim().length() > 0) {
            qwhere = qwhere + " AND " + ContactsContract.Contacts._ID + " = ?";
            qwhereargs[cntarg] = contactid;
            cntarg++;
        }
        if (filter != null && filter.trim().length() > 0) {
            qwhere = qwhere + " AND UPPER(" + ContactsContract.Contacts.DISPLAY_NAME + ") LIKE ? ";
            qwhereargs[cntarg] = "%" + filter.trim().toUpperCase() + "%";
        }

        //Order
        if (order != null && order.trim().length() > 0)
            qorder = order;
        else
            qorder = ContactsContract.Contacts.DISPLAY_NAME;

        if (limit > 0) {
            qorder = qorder + " LIMIT " + String.valueOf(pageIn) + ", " + String.valueOf(limit);
        }

        cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, qwhere, qwhereargs, qorder);

        outlist = new modContact[cursor.getCount()];
        int cntcont = pageIn;
        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, ContactsContract.CommonDataKinds.Phone.NUMBER);
                String lastnum = "";
                if (pCur != null && !pCur.isClosed())
                    while (pCur.moveToNext()) {
                        String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phone = phone.replaceAll("[^0-9]", "");
                        phone = phone.replaceAll(" ", "");
                        if (!phone.trim().equals(lastnum)) {
                            lastnum = phone;
                            modContact contact = new modContact();
                            contact.contactindexhelper = String.valueOf(cntcont);
                            contact.cid = id;
                            contact.phone = phone;
                            contact.name = pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                            byte[] cico = null;
                            try {
                                cico = queryContactImage(ctx, cursor.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_ID)));
                            }catch (Exception e){

                            }
                            if (cico != null) {
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                contact.icon = BitmapFactory.decodeByteArray(cico, 0, cico.length, options);
                            }

                            //E-mail
                            Cursor mCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                            while (mCur.moveToNext()) {
                                String m = mCur.getString(mCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                                if (m != null && m.trim().length() > 0) {
                                    contact.email = m;
                                    break;
                                }
                            }
                            mCur.close();
                            tmpu.add(contact);
                        }
                    }
                pCur.close();
                cntcont++;
            } while (cursor.moveToNext());
        }

        if (tmpu != null && tmpu.size() > 0) {
            outlist = new modContact[tmpu.size()];
            int cnt = 0;
            for (modContact c : tmpu) {
                outlist[cnt] = c;
                cnt++;
            }

        }
        return outlist;
    }


    //----------------------------------------------------------------------------------------------
    // GET CONTACT BY NUMBER
    //----------------------------------------------------------------------------------------------

    public static modContact getContactByNumber(Context ctx, String number) {
        number = number.replaceAll("[^0-9]", "");
        number = number.replaceAll(" ", "");

        ArrayList<modContact> tmpu = new ArrayList<modContact>();
        ContentResolver cr = ctx.getContentResolver();


        if (number.length() > 9) {
            number = number.substring(number.length() - 9);
        }else{

        }
        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE ?", new String[]{"%" + number}, ContactsContract.Contacts.DISPLAY_NAME);

        if (cursor.moveToFirst()) {
            String id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            modContact[] aout = getContactList(ctx, id, null, null, 10, 0);
            if (aout != null && aout.length > 0)
                return aout[0];
        }

        return null;
    }


    //----------------------------------------------------------------------------------------------
    // GET CONTACT BITMAP BYTES
    //----------------------------------------------------------------------------------------------

    private static byte[] queryContactImage(Context ctx, int imageDataRow) {
        byte[] imageBytes = null;
        Cursor c = null;
        try {
            c = ctx.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{
                    ContactsContract.CommonDataKinds.Photo.PHOTO
            }, ContactsContract.Data._ID + "=?", new String[]{
                    Integer.toString(imageDataRow)
            }, null);

            if (c != null) {
                if (c.moveToFirst()) {
                    imageBytes = c.getBlob(0);
                }
                c.close();
            }
        } catch (Exception ex) {
            if (c != null) c.close();
        } finally {
            if (c != null) c.close();
        }
        return imageBytes;
    }



    //----------------------------------------------------------------------------------------------
    // GET CALL HISTORY LIST
    //----------------------------------------------------------------------------------------------


    public static modContact[] getCallList(Context ctx, String filter, String order, int limit, int pageIn) {

        modContact[] outcontacts = null;


        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED)
            return null;

        String qorder = null;
        String qwhere = null;

        if (filter != null && filter.trim().length() > 0) {
            qwhere = CallLog.Calls.CACHED_NAME + " LIKE '%" + filter + "%' ";
            qwhere = qwhere + " OR " + CallLog.Calls.NUMBER + " LIKE '%" + filter + "%' ";
        }
        //Order
        if (order != null && order.trim().length() > 0)
            qorder = order;
        else
            qorder = CallLog.Calls.DATE + " DESC";
        if (limit > 0) {
            qorder = qorder + " LIMIT " + String.valueOf(pageIn) + ", " + String.valueOf(limit);
        }

        ContentResolver cr = ctx.getContentResolver();
        Cursor cursor = cr.query(CallLog.Calls.CONTENT_URI, null,
                qwhere, null, qorder);
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);
        int cname = cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        int cphid = cursor.getColumnIndex(CallLog.Calls.CACHED_PHOTO_ID);



        if (cursor != null && cursor.getCount() > 0) {
            outcontacts = new modContact[cursor.getCount()];
            int cnt = 0;
            while (cursor.moveToNext()) {
                String phNumber = cursor.getString(number);
                String callType = cursor.getString(type);
                String callDate = cursor.getString(date);
                String name = cursor.getString(cname);
                String photoid = cursor.getString(cphid);
                ZonedDateTime callDT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(Long.valueOf(callDate)), ZoneId.systemDefault());
                String callDuration = cursor.getString(duration);
                String dir = null;
                int dircode = Integer.parseInt(callType);
                modContact c = getContactByNumber(ctx,phNumber);
                switch (dircode) {
                    case CallLog.Calls.OUTGOING_TYPE:
                        dir = "OUTGOING";
                        break;

                    case CallLog.Calls.INCOMING_TYPE:
                        dir = "INCOMING";
                        break;

                    case CallLog.Calls.MISSED_TYPE:
                        dir = "MISSED";
                        break;
                }
                outcontacts[cnt] = new modContact();
                if(c != null) {
                    outcontacts[cnt].cid = c.cid;
                    outcontacts[cnt].name = c.name;
                }else{
                    outcontacts[cnt].name = name;
                }
                outcontacts[cnt].phone = phNumber;
                outcontacts[cnt].call_dir = dir;
                outcontacts[cnt].dtcallorig = callDT.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                outcontacts[cnt].dtcall = callDT.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
                try {
                    byte[] cico = queryContactImage(ctx, Integer.parseInt(photoid));
                    if (cico != null) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        outcontacts[cnt].icon = BitmapFactory.decodeByteArray(cico, 0, cico.length, options);
                    }
                } catch (Exception e) {

                }

                cnt++;
            }
        }
        cursor.close();
        return outcontacts;

    }

    //----------------------------------------------------------------------------------------------
    // CLEAR CALL LOG
    //----------------------------------------------------------------------------------------------

    public static boolean clearCallLog(Context ctx) {

        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
            try {
                ctx.getContentResolver().delete(android.provider.CallLog.Calls.CONTENT_URI, null, null);
            } catch (Exception e) {
                String em = e.getMessage();
            }
            return true;
        }

        return false;

    }



    //----------------------------------------------------------------------------------------------
    // FORMAT NUMBER
    //----------------------------------------------------------------------------------------------

    public static String formatNumber(String number) {
        if(number == null || number.trim().length() <= 0)
            return number;

        switch(number.length()){
            case 6:
                return number.substring(0,3) + "-" + number.substring(3);
            case 7:
                return number.substring(0,3) + "-" + number.substring(3);
            case 8:
                return number.substring(0,4) + "-" + number.substring(4);
            case 9:
                if(number.substring(0,1).equals("0"))
                    return "("+number.substring(1,3) + ") " + number.substring(3,6) + "-" + number.substring(6);
                return number.substring(0,5) + "-" + number.substring(5);
            case 10:
                if(number.substring(0,1).equals("0"))
                    return "("+number.substring(1,3) + ") " + number.substring(3,6) + "-" + number.substring(6);
                return "("+number.substring(0,2) + ") " + number.substring(2,6) + "-" + number.substring(6);
            case 11:
                if(number.substring(0,1).equals("0"))
                    return "("+number.substring(1,3) + ") " + number.substring(3,7) + "-" + number.substring(7);
                if(number.substring(0,2).equals("55"))
                    return "+" + number.substring(0,2) + " ("+number.substring(2,4) + ") " + number.substring(4,7) + "-" + number.substring(7);
                return "("+number.substring(0,2) + ") " + number.substring(2,7) + "-" + number.substring(7);
            case 12:
                if(number.substring(0,1).equals("0"))
                    return "("+number.substring(1,3) + ") " + number.substring(3,8) + "-" + number.substring(8);
                if(number.substring(0,2).equals("55"))
                    return "+" + number.substring(0,2) + " ("+number.substring(2,4) + ") " + number.substring(4,8) + "-" + number.substring(8);
                break;
            case 13:
                if(number.substring(0,1).equals("0"))
                    return number.substring(0,3) + " (" + number.substring(3,5) + ") " + number.substring(5,9) + "-" + number.substring(9);
                if(number.substring(0,2).equals("55"))
                    return "+" + number.substring(0,2) + " ("+number.substring(2,4) + ") " + number.substring(4,9) + "-" + number.substring(9);
                break;
            case 14:
                if(number.substring(0,1).equals("0"))
                    return number.substring(0,3) + " (" + number.substring(3,5) + ") " + number.substring(5,10) + "-" + number.substring(10);
                break;

        }

        return number;

    }

}