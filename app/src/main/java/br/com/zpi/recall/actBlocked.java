package br.com.zpi.recall;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;

import br.com.zpi.recall.db.RCDBException;
import br.com.zpi.recall.db.dbContacts;
import br.com.zpi.recall.extendables.MyActivity;
import br.com.zpi.recall.extendables.MyBaseListLineAdapter;
import br.com.zpi.recall.model.modContact;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

public class actBlocked extends MyActivity {

    private EditText inpNum = null;
    private ImageView imgAdd = null;
    private ListView lstBlocked = null;
    private TextView txtNoRes = null;

    private static int resPage = 0;
    private boolean resEnd = false;
    private modContact[] blocked = null;
    private static final int PAGE_LIMIT = 20;

    //##############################################################################################
    // CREATE
    //##############################################################################################

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_blocked);

        inpNum = (EditText) findViewById(R.id.inpNum);
        imgAdd = (ImageView) findViewById(R.id.imgAdd);
        lstBlocked = (ListView) findViewById(R.id.lstBlocked);
        txtNoRes = (TextView) findViewById(R.id.txtNoRes);

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbContacts dbC = new dbContacts(getBaseContext());

                if(inpNum.getText().toString().trim().length() <= 0){

                    return;
                }
                int count = inpNum.getText().toString().length() - inpNum.getText().toString().replace("*", "").length();
                if(count > 1) {
                    new AlertDialog.Builder(actBlocked.this)
                            .setTitle(actBlocked.this.getResources().getText(R.string.blockallow_err).toString())
                            .setMessage(actBlocked.this.getResources().getText(R.string.blockallow_err_asterisk).toString())
                            .setPositiveButton(R.string.ok, null)
                            .setNegativeButton(null, null)
                            .setIcon(android.R.drawable.ic_dialog_alert).show();
                    return;
                }
                try{
                    dbC.blockNum(inpNum.getText().toString().trim(), null);
                    inpNum.setText("");
                    loadBlocked(true);
                }catch (RCDBException ex){

                }

            }
        });

        // TEXT FILTER CHANGE LISTENER
        inpNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                loadBlocked(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        loadBlocked(true);
    }


    //##############################################################################################
    //##############################################################################################
    // CLASS FUNCTIONS
    //##############################################################################################
    //##############################################################################################

    //----------------------------------------------------------------------------------------------
    // LOAD BLOCKED
    //----------------------------------------------------------------------------------------------

    public void loadBlocked(boolean resetpaging) {
        txtNoRes.setVisibility(View.GONE);

        dbContacts dbC = new dbContacts(this);

        String filter = null;

        if (inpNum.getText().toString().trim().length() > 0)
            filter = inpNum.getText().toString();

        if (resetpaging) {
            resPage = 0;
            resEnd = false;
            lstBlocked.setAdapter(null);
            lstBlocked.bringToFront();
        }
        if (resEnd) {
            if(resPage == 0 && (blocked == null || blocked.length == 0))
                txtNoRes.setVisibility(View.VISIBLE);
            return;
        }

        try {
            blocked = dbC.listBlocked(filter, null, PAGE_LIMIT, resPage);
        }catch (RCDBException dbe){
        }


        if(blocked != null && blocked.length > 0){

            ArrayList<modContact> a = new ArrayList<modContact>();
            if (resPage == 0) {
                for (modContact c : blocked)
                    if (c.phone != null && c.phone.trim().length() > 0)
                        a.add(c);
                MyBaseListLineAdapter adFiles = new MyBaseListLineAdapter(this, a) {
                    @Override
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        modContact contact = (modContact) getContent().get(i);
                        if (!resEnd && i > 0 && i >= (getContent().size() - 1) && getContent().size() >= PAGE_LIMIT) {
                            resPage = i;
                            loadBlocked(false);
                        }
                        return drawBlocked(contact);
                    }
                };
                lstBlocked.setAdapter(adFiles);
                lstBlocked.refreshDrawableState();
            } else {
                MyBaseListLineAdapter ad = (MyBaseListLineAdapter) lstBlocked.getAdapter();
                a = ad.getContent();
                for (modContact c : blocked)
                    if (c.phone != null && c.phone.trim().length() > 0)
                        a.add(c);
                ad.setContent(a);
                ad.notifyDataSetChanged();
            }


        }else{
            if (resPage == 0)
                txtNoRes.setVisibility(View.VISIBLE);
            resEnd = true;
        }
    }

    //----------------------------------------------------------------------------------------------
    // LOAD BLOCKED
    //----------------------------------------------------------------------------------------------
    private RelativeLayout drawBlocked(modContact c){
        LayoutInflater inf = getLayoutInflater();
        RelativeLayout lay = (RelativeLayout) inf.inflate(R.layout.line_blocked, null);

        TextView txtName = (TextView) lay.findViewById(R.id.txtName);
        TextView txtPhone = (TextView) lay.findViewById(R.id.txtPhone);
        ImageView imgProfPic = (ImageView) lay.findViewById(R.id.imgProfPic);
        ImageView imgBlock = (ImageView) lay.findViewById(R.id.imgBlock);

        if (c.name != null && c.name.trim().length() > 0)
            txtName.setText(c.name);
        else if (c.phone != null && c.phone.trim().length() > 0)
            txtName.setText(c.phone);
        else
            txtName.setText("--");

        if (c.phone != null && c.phone.trim().length() > 0) {
            txtPhone.setText(c.phone);

        } else
            txtPhone.setText("--");

        if (c.icon != null)
            imgProfPic.setImageBitmap(c.icon);

        imgBlock.setTag("imb" + c.phone);
        imgBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbContacts dbC = new dbContacts(getBaseContext());
                String num = view.getTag().toString().substring(3);
                try{
                    dbC.releaseNum(num);
                    loadBlocked(true);
                }catch (RCDBException exc){

                }

            }
        });

        return lay;
    }

}
