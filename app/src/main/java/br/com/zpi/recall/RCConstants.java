package br.com.zpi.recall;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
public class RCConstants {

    public static final String RCCLOSE = "endRC";
    public static final String REFRESHLAYOUT = "refrLay";
    public static final String recpath = "/rcrec";
    public static final String recext = "amr";
    public static final String rectimestamp = "yyyyMMddHHmmss";
    public static final String gdriverecpath = "recall/recordings/all";
}
