package br.com.zpi.recall.extendables;

	/*
	 * Copyright 2018 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */
	
    
import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Patterns;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;

import br.com.zpi.recall.R;
import br.com.zpi.recall.RCConstants;
import br.com.zpi.recall.RCRecorderService;

/**
 * Created by leonardo on 01/11/16.
 */

public class MyActivity extends AppCompatActivity {
    private static final int PERM_PHONESTATE = 1;
    private static final int REQUEST_CODE = 0;
    private ComponentName mAdminName;
    public MyPopup mdiag = null;
    public static SharedPreferences prefs = null;
    public static SharedPreferences.Editor prefeditor = null;
    public Typeface fontRobotoLight = null;
    public Typeface fontRobotoNormal = null;
    public Typeface fontRobotoBold = null;
    public Typeface fontRobotoItalic = null;

    public static GoogleApiClient mGoogleApiClient = null;
    public static final int RESOLVE_CONNECTION_REQUEST_CODE = 60061;
    public static DriveFolder gdriveRecAll = null;
    private final BroadcastReceiver endReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    private final BroadcastReceiver refrLay = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            recreate();
        }
    };

    public RCRecorderService myService = null;
    public String userEmail = null;

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceDisconnected(ComponentName name) {
            myService = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            RCRecorderService.LocalBinder mLocalBinder = (RCRecorderService.LocalBinder) service;
            myService = mLocalBinder.getService();
        }
    };

    //##############################################################################################
    // CREATE
    //##############################################################################################

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerReceiver(endReceiver, new IntentFilter(RCConstants.RCCLOSE));
        registerReceiver(refrLay, new IntentFilter(RCConstants.REFRESHLAYOUT));
        // Load FONTS
        fontRobotoLight = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Light.ttf");
        fontRobotoNormal = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Regular.ttf");
        fontRobotoBold = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Bold.ttf");
        fontRobotoItalic = Typeface.createFromAsset(getAssets(), "roboto/Roboto-Italic.ttf");
        prefs = getSharedPreferences("recall",
                Context.MODE_PRIVATE);
        prefeditor = prefs.edit();

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_BOOT_COMPLETED) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALL_LOG) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.PROCESS_OUTGOING_CALLS) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.CAPTURE_AUDIO_OUTPUT) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MyActivity.this,
                        new String[]{Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.RECEIVE_BOOT_COMPLETED,
                                Manifest.permission.WRITE_CALL_LOG,
                                Manifest.permission.READ_CONTACTS,
                                Manifest.permission.CALL_PHONE,
                                Manifest.permission.READ_CALL_LOG,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.PROCESS_OUTGOING_CALLS,
                                Manifest.permission.CAPTURE_AUDIO_OUTPUT,
                                Manifest.permission.GET_ACCOUNTS,
                                Manifest.permission.INTERNET,
                                Manifest.permission.SEND_SMS,
                                Manifest.permission.RECORD_AUDIO},
                        PERM_PHONESTATE);
            }
        } else {

        }

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.rc_bar));
            window.setNavigationBarColor(getResources().getColor(R.color.rc_bar));
        }
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                userEmail = account.name;
                break;
            }
        }

        initService();
    }


    //##############################################################################################
    // ON RESUME
    //##############################################################################################

    @Override
    protected void onResume() {
        super.onResume();
        Intent iPlayer = new Intent(this, RCRecorderService.class);
        getApplicationContext().bindService(iPlayer, mConnection, Context.BIND_ADJUST_WITH_ACTIVITY);

        mdiag = new MyPopup(this);
    }

    //##############################################################################################
    // PAUSE
    //##############################################################################################

    @Override
    protected void onPause() {
        super.onPause();
        if (mConnection != null) getApplicationContext().unbindService(mConnection);
        //System.gc();
    }

    //##############################################################################################
    // DESTROY
    //##############################################################################################
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refrLay);
        unregisterReceiver(endReceiver);
    }

    //##############################################################################################
    // ACTIVITY RESULT
    //##############################################################################################

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (REQUEST_CODE == requestCode) {
            Intent intent = new Intent(MyActivity.this, RCRecorderService.class);
            startService(intent);
        } else if (RESOLVE_CONNECTION_REQUEST_CODE == requestCode) {
            if (resultCode == RESULT_OK) {
                mGoogleApiClient.connect();
            }else{
                mdiag.displayMessage(getResources().getText(R.string.gdrive_errorconnecting).toString(), 2);
            }
        }
    }


    //##############################################################################################
    // PERMISSION RESULT
    //##############################################################################################

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERM_PHONESTATE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED
                        && grantResults[3] == PackageManager.PERMISSION_GRANTED
                        && grantResults[4] == PackageManager.PERMISSION_GRANTED
                        && grantResults[5] == PackageManager.PERMISSION_GRANTED
                        && grantResults[6] == PackageManager.PERMISSION_GRANTED
                        && grantResults[7] == PackageManager.PERMISSION_GRANTED
                        && grantResults[8] == PackageManager.PERMISSION_GRANTED
                        && grantResults[9] == PackageManager.PERMISSION_GRANTED
                        && grantResults[10] == PackageManager.PERMISSION_GRANTED
                        && grantResults[11] == PackageManager.PERMISSION_GRANTED
                        && grantResults[12] == PackageManager.PERMISSION_GRANTED
                        && grantResults[13] == PackageManager.PERMISSION_GRANTED) {

                }
            }

        }
    }


    //##############################################################################################
    //##############################################################################################
    // CLASS FUNCTIONS
    //##############################################################################################
    //##############################################################################################

    //----------------------------------------------------------------------------------------------
    // ENABLE GOOGLE DRIVE
    //----------------------------------------------------------------------------------------------
    public void enableGDrive() {
        try {
            if (mGoogleApiClient == null)
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addApi(Drive.API)
                        .addScope(Drive.SCOPE_FILE)
                        .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                            @Override
                            public void onConnected(@Nullable Bundle bundle) {
                                preCheckRecallFolder();
                            }

                            @Override
                            public void onConnectionSuspended(int i) {

                            }
                        })
                        .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                            @Override
                            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                                if (connectionResult.hasResolution()) {
                                    try {
                                        connectionResult.startResolutionForResult(MyActivity.this, RESOLVE_CONNECTION_REQUEST_CODE);
                                    } catch (IntentSender.SendIntentException e) {

                                    }
                                } else {
                                    GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), MyActivity.this, 0).show();
                                }
                            }
                        })
                        .build();
        }catch (Exception e){
            String ms = e.getMessage();
        }
    }

    //----------------------------------------------------------------------------------------------
    // DISABLE / DISCONNECT FROM GOOGLE DRIVE
    //----------------------------------------------------------------------------------------------

    public void disableGDrive() {
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();

    }

    //----------------------------------------------------------------------------------------------
    // CONNECT TO GOOGLE DRIVE
    //----------------------------------------------------------------------------------------------
    public void connectGDrive() {
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
    }

    //----------------------------------------------------------------------------------------------
    // INITIALIZE SERVICE
    //----------------------------------------------------------------------------------------------
    private void initService() {
        try {
            if (!isMyServiceRunning(RCRecorderService.class)) {
                Intent intent = new Intent(MyActivity.this,
                        RCRecorderService.class);
                startService(intent);
            }
        } catch (Exception e) {

        }
    }

    //----------------------------------------------------------------------------------------------
    // CHECK OR CREATE RECALL FOLDER ON GDRIVE
    //----------------------------------------------------------------------------------------------
    private static int curGFolderIndex = 0;

    public void preCheckRecallFolder(){
        Drive.DriveApi.requestSync(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                curGFolderIndex = 0;
                checkcreateRecallFolder();
            }
        });
    }

    public void checkcreateRecallFolder(){
        String[] folders = null;
        if(RCConstants.gdriverecpath.indexOf("/") > 0){
            folders = RCConstants.gdriverecpath.split("/");
            if(curGFolderIndex > folders.length)
                return;
        }else{
            folders = new String[]{RCConstants.gdriverecpath};
            if(curGFolderIndex > 0)
                return;
        }

        try {
            Query query = new Query.Builder()
                    .addFilter(Filters.and(Filters.eq(
                            SearchableField.TITLE, folders[curGFolderIndex]),
                            Filters.eq(SearchableField.TRASHED, false)))
                    .build();
            Drive.DriveApi.query(mGoogleApiClient, query)
                    .setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
                        @Override
                        public void onResult(DriveApi.MetadataBufferResult result) {
                            String[] folders = null;
                            if(RCConstants.gdriverecpath.indexOf("/") > 0){
                                folders = RCConstants.gdriverecpath.split("/");
                            }else{
                                folders = new String[]{RCConstants.gdriverecpath};
                            }
                            if (!result.getStatus().isSuccess()) {
                                mdiag.displayMessage(getResources().getText(R.string.gdrive_errorcreatingfolder).toString(), 2);
                            } else {
                                boolean isFound = false;
                                for(Metadata m : result.getMetadataBuffer()) {
                                    if (m.getTitle().equals(folders[curGFolderIndex])) {
                                        isFound = true;
                                        gdriveRecAll = Drive.DriveApi.getAppFolder(mGoogleApiClient);
                                        curGFolderIndex++;
                                        checkcreateRecallFolder();
                                        break;
                                    }
                                }
                                if(!isFound) {
                                    MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                                            .setTitle(folders[curGFolderIndex])
                                            .build();
                                    DriveFolder drv = Drive.DriveApi.getRootFolder(mGoogleApiClient);
                                    if(gdriveRecAll != null)
                                        drv = gdriveRecAll;
                                    drv.createFolder(mGoogleApiClient, changeSet)
                                            .setResultCallback(new ResultCallback<DriveFolder.DriveFolderResult>() {
                                                @Override
                                                public void onResult(DriveFolder.DriveFolderResult result) {
                                                    if (!result.getStatus().isSuccess()) {
                                                        mdiag.displayMessage(getResources().getText(R.string.gdrive_errorcreatingfolder).toString(), 2);
                                                    }else{
                                                        gdriveRecAll = result.getDriveFolder();
                                                        curGFolderIndex++;
                                                        checkcreateRecallFolder();
                                                    }
                                                }
                                            });
                                }
                            }
                        }
                    });
        }catch (Exception e){
            mdiag.displayMessage(getResources().getText(R.string.gdrive_errorcreatingfolder).toString(), 2);
        }
    }

    //----------------------------------------------------------------------------------------------
    // UPLOAD FILE GDRIVE
    //----------------------------------------------------------------------------------------------
    private static String gdrive_fpath = null;

    public boolean gdriveUpload(final String filepath){

        if (!prefs.getString("gdrive", "").trim().toUpperCase().equals("X")) {
            mdiag.displayMessage(getResources().getText(R.string.gdrive_errorgdrivedisabled).toString(), 2);
            return false;
        }
        if(mGoogleApiClient == null || gdriveRecAll == null){
            mdiag.displayMessage(getResources().getText(R.string.gdrive_errorcreatingfolder).toString(), 2);
            return false;
        }

        // UPLOAD
        Drive.DriveApi.newDriveContents(mGoogleApiClient).setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>(){
            @Override
            public void onResult(@NonNull DriveApi.DriveContentsResult driveContentsResult) {

                OutputStream outputStream = driveContentsResult.getDriveContents().getOutputStream();
                try {
                    InputStream inputStream = getContentResolver().openInputStream(Uri.parse(filepath));

                    if (inputStream != null) {
                        byte[] data = new byte[1024];
                        while (inputStream.read(data) != -1) {
                            outputStream.write(data);
                        }
                        inputStream.close();
                    }

                    outputStream.close();
                } catch (IOException e) {

                }

                MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                        .setTitle(filepath)
                        .setMimeType("audio/amr")
                        .setStarred(true).build();
                gdriveRecAll.createFile(mGoogleApiClient,changeSet,driveContentsResult.getDriveContents());
            }
        });

        return false;
    }



    //----------------------------------------------------------------------------------------------
    // CHECK SERVICES
    //----------------------------------------------------------------------------------------------

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


}
