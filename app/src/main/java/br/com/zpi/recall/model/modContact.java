package br.com.zpi.recall.model;

import android.graphics.Bitmap;

	/*
	 * Copyright 2018 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */
	

public class modContact {
    public String name = null;
    public String email = null;
    public String phone = null;
    public String phone_form = null;
    public Bitmap icon = null;
    public String cid = null;

    public String call_dir = null;
    public String dtcall = null;
    public String dtview = null;
    public String notif = null;

    public String contactindexhelper = null;
    public String wildcard = null;
    public String dtcallorig = null;
}
