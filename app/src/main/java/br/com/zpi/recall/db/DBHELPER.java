package br.com.zpi.recall.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
public class DBHELPER extends SQLiteOpenHelper {
    public DBHELPER(Context ctx) {
        super(ctx, "blist.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE blocked (number TEXT UNIQUE, contactid TEXT, dtadded TEXT)");
        db.execSQL("CREATE TABLE allowed (number TEXT UNIQUE, contactid TEXT, dtadded TEXT)");
        db.execSQL("CREATE TABLE record (number TEXT UNIQUE, contactid TEXT, dtadded TEXT)");
        db.execSQL("CREATE TABLE blocked_log (number TEXT, dtcall TEXT , contactid TEXT, contactname TEXT, dtview TEXT, notif TEXT, PRIMARY KEY (number, dtcall))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE blocked");
        db.execSQL("DROP TABLE allowed");
        db.execSQL("DROP TABLE record");
        db.execSQL("DROP TABLE blocked_log");
        // create all tables again
        db.execSQL("CREATE TABLE blocked (number TEXT UNIQUE, contactid TEXT, dtadded TEXT)");
        db.execSQL("CREATE TABLE allowed (number TEXT UNIQUE, contactid TEXT, dtadded TEXT)");
        db.execSQL("CREATE TABLE record (number TEXT UNIQUE, contactid TEXT, dtadded TEXT)");
        db.execSQL("CREATE TABLE blocked_log (number TEXT, dtcall TEXT , contactid TEXT, contactname TEXT, dtview TEXT, notif TEXT, PRIMARY KEY (number, dtcall))");

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
