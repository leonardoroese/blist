package br.com.zpi.recall.extendables;

import android.app.Activity;
import android.os.Handler;
import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.com.zpi.recall.R;

	/*
	 * Copyright 2018 Leonardo Germano Roese
	 * 
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 * 
	 * http://www.apache.org/licenses/LICENSE-2.0
	 * 
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

public class MyPopup extends PopupWindow {

    private static Handler hand = null;
    private static Runnable runn = null;
    private static Activity activity = null;
    private static RelativeLayout lay = null;

    public MyPopup(Activity act){
        super();
        activity = act;
        LayoutInflater inf = activity.getLayoutInflater();
        lay = (RelativeLayout) inf.inflate(R.layout.mydialog_base, null);
        RelativeLayout.LayoutParams LP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, activity.getResources().getDimensionPixelOffset(R.dimen.mydialog_h));
        lay.setLayoutParams(LP);
        setContentView(lay);


    }

    public void displayMessage(String message, int secs){
        TextView txtMSG = (TextView) lay.findViewById(R.id.txtMSG);
        txtMSG.setText(message);
        setWidth(RelativeLayout.LayoutParams.MATCH_PARENT);
        setHeight(RelativeLayout.LayoutParams.WRAP_CONTENT);
        if(hand != null){
            hand.removeCallbacks(runn);
            dismiss();
        }

        showAtLocation(lay, Gravity.TOP, 10, 10);

        hand = new Handler();
        runn = new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        };
        hand.postDelayed(runn, secs * 1000);
    }

}
