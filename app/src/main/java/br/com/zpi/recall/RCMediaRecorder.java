package br.com.zpi.recall;

import android.media.MediaRecorder;
import android.os.Environment;

import java.io.File;

	/*
	 * Copyright 2018 Leonardo Germano Roese
	 *
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 *
	 * http://www.apache.org/licenses/LICENSE-2.0
	 *
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

public class RCMediaRecorder extends MediaRecorder {

    public boolean recording = false;
    private File audiofile = null;


    public RCMediaRecorder() {
        recording = false;
    }

    public void start(String filename) {
        if (!recording) {
            File sampleDir = new File(Environment.getExternalStorageDirectory(), RCConstants.recpath);
            if (!sampleDir.exists()) {
                sampleDir.mkdirs();
            }
            try {
                audiofile = new File(sampleDir, filename + "." + RCConstants.recext);
                audiofile.setWritable(true);
                audiofile.setReadable(true);
                //audiofile = File.createTempFile(filename, ".amr", sampleDir);
                setAudioSource(AudioSource.VOICE_COMMUNICATION);
                setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
                setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                setOutputFile(audiofile.getAbsolutePath());
                prepare();
                start();
                recording = true;
            } catch (Exception e) {
                try {
                    setAudioSource(AudioSource.MIC);
                    prepare();
                    start();
                    recording = true;
                } catch (Exception ex) {
                    String em = e.getMessage();
                }

            }

        }
    }


    @Override
    public void stop() throws IllegalStateException {
        super.stop();
        recording = false;
    }

    @Override
    public void reset() {
        super.reset();
        recording = false;
    }

    @Override
    public void release() {
        super.release();
        recording = false;
    }


}
