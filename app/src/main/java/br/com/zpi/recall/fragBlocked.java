package br.com.zpi.recall;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;

import br.com.zpi.recall.db.RCDBException;
import br.com.zpi.recall.db.dbBlock;
import br.com.zpi.recall.db.dbContacts;
import br.com.zpi.recall.extendables.MyBaseListLineAdapter;
import br.com.zpi.recall.extendables.MyTask;
import br.com.zpi.recall.model.modContact;
import br.com.zpi.recall.tools.Generic;
import br.com.zpi.recall.tools.RCContacts;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

public class fragBlocked extends Fragment {
    private ListView lstBlocked = null;
    private Context myContext = null;
    private RelativeLayout mainLayout = null;
    private actContacts mainActivity = null;
    private SwipeRefreshLayout swipeBlocked = null;
    private TextView txtNoRes = null;
    private static int RESULT_LIMIT = 20;
    private static int resPage = 0;
    private boolean resEnd = false;
    private static final int CONTACTS_RES_REFRESH = 44;
    private static final int CONTACTS_REQ_DISPLAY = 45;
    private EditText inpSearch = null;
    private MyTask task = null;

    public fragBlocked() {
        // Required empty public constructor
    }


    //##############################################################################################
    // CREATE VIEW
    //##############################################################################################

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_blocked, container, false);
        myContext = getActivity().getBaseContext();
        mainActivity = (actContacts) getActivity();
        return mainLayout;
    }


    //##############################################################################################
    // VIEW CREATED
    //##############################################################################################

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lstBlocked = (ListView) mainLayout.findViewById(R.id.lstBlocked);
        swipeBlocked = (SwipeRefreshLayout) mainLayout.findViewById(R.id.swipeBlocked);
        txtNoRes = (TextView) mainLayout.findViewById(R.id.txtNoRes);
        ImageView imgDelB = (ImageView) mainLayout.findViewById(R.id.imgDelB);
        inpSearch = (EditText) mainLayout.findViewById(R.id.inpSearch);

        swipeBlocked.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadBlocked(true);
            }
        });

        imgDelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AlertDialog.Builder(getActivity())
                        .setTitle(getActivity().getResources().getText(R.string.blocked_dialog_title_clean).toString())
                        .setMessage(getActivity().getResources().getText(R.string.blocked_dialog_text_clean).toString())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (whichButton == Dialog.BUTTON_POSITIVE) {
                                    dbBlock dbB = new dbBlock(mainActivity);
                                    try {
                                        dbB.clearAll();
                                        loadBlocked(true);
                                    } catch (RCDBException e) {

                                    }
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();

            }
        });

        // TEXT FILTER CHANGE LISTENER
        inpSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                loadBlocked(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        loadBlocked(true);
    }



    //##############################################################################################
    //##############################################################################################
    // CLASS FUNCTIONS
    //##############################################################################################
    //##############################################################################################


    //----------------------------------------------------------------------------------------------
    // LOAD BLOCKED NUMBERS
    //----------------------------------------------------------------------------------------------

    public void loadBlocked(boolean resetpaging) {
        if(task != null && task.getStatus() == AsyncTask.Status.RUNNING){
            if(!resetpaging)
                return;
            else
                task.cancel(true);
        }

        if (resetpaging) {
            resPage = 0;
            lstBlocked.setAdapter(null);
            lstBlocked.bringToFront();
            resEnd = false;
        }
        if (resEnd) {
            swipeBlocked.setRefreshing(false);
            return;
        }
        txtNoRes.setVisibility(View.GONE);


        task = new MyTask() {
            private String filter = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if (inpSearch.getText().toString().trim().length() > 0)
                    filter = inpSearch.getText().toString();
                swipeBlocked.setRefreshing(true);
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                dbBlock dbB = new dbBlock(mainActivity);

                try{
                    dbB.setNotifViewed();
                }catch (RCDBException e){

                }
                try {
                    modContact[] c = dbB.listLogs(filter, null, RESULT_LIMIT, resPage);
                    return c;
                } catch (Exception e) {

                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                RCReceiver.displayNotifB(getContext());
                if(o != null){
                    modContact[] cts = (modContact[]) o;
                    ArrayList<modContact> a = new ArrayList<modContact>();
                    if (resPage == 0) {
                        for (modContact c : cts)
                            if (c.phone != null && c.phone.trim().length() > 0)
                                a.add(c);

                        MyBaseListLineAdapter adFiles = new MyBaseListLineAdapter(getActivity(), a) {
                            @Override
                            public View getView(int i, View view, ViewGroup viewGroup) {
                                modContact contact = (modContact) getContent().get(i);
                                if (!resEnd && i > 0 && i >= (getContent().size() - 3) && getContent().size() >= RESULT_LIMIT) {
                                    resPage = i;
                                    loadBlocked(false);
                                }
                                return drawContact(contact, i);
                            }
                        };
                        lstBlocked.setAdapter(adFiles);
                    } else {
                        MyBaseListLineAdapter ad = (MyBaseListLineAdapter) lstBlocked.getAdapter();
                        a = ad.getContent();
                        for (modContact c : cts)
                            if (c.phone != null && c.phone.trim().length() > 0)
                                a.add(c);
                        ad.setContent(a);
                        ad.notifyDataSetChanged();
                    }
                }else{
                    if (resPage == 0)
                        txtNoRes.setVisibility(View.VISIBLE);
                    resEnd = true;
                }
                swipeBlocked.setRefreshing(false);
            }
        };

        task.execute();
    }


    //----------------------------------------------------------------------------------------------
    // DRAW LINE
    //----------------------------------------------------------------------------------------------

    public RelativeLayout drawContact(modContact contact, int index) {
        dbContacts dbC = new dbContacts(getActivity());

        LayoutInflater inf = getActivity().getLayoutInflater();
        RelativeLayout lay = (RelativeLayout) inf.inflate(R.layout.line_log, null);

        TextView txtName = (TextView) lay.findViewById(R.id.txtName);
        TextView txtPhone = (TextView) lay.findViewById(R.id.txtPhone);
        TextView txtDate = (TextView) lay.findViewById(R.id.txtDate);
        ImageView imgBlock = (ImageView) lay.findViewById(R.id.imgBlock);
        ImageView imgRec = (ImageView) lay.findViewById(R.id.imgRec);
        ImageView imgProfPic = (ImageView) lay.findViewById(R.id.imgProfPic);
        ImageView imgNew = (ImageView) lay.findViewById(R.id.imgNew);
        RelativeLayout layIBlock = (RelativeLayout) lay.findViewById(R.id.layIBlock);
        RelativeLayout layIRec = (RelativeLayout) lay.findViewById(R.id.layIRec);

        lay.setTag("ruser" + contact.cid);
        lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = view.getTag().toString().substring(5);
                if (id != null && id.trim().length() > 0) {
                    Intent i = new Intent(getActivity(), actUser.class);
                    i.putExtra("user_id", id);
                    startActivityForResult(i, CONTACTS_REQ_DISPLAY);
                }
            }
        });


        if (contact.name != null && contact.name.trim().length() > 0)
            txtName.setText(contact.name);
        else if (contact.phone != null && contact.phone.trim().length() > 0)
            txtName.setText(contact.phone);
        else
            txtName.setText("--");

        if (contact.dtcall != null && contact.dtcall.trim().length() > 0)
            txtDate.setText(contact.dtcall);
        else
            txtDate.setText("--");

        if (contact.phone != null && contact.phone.trim().length() > 0) {
            txtPhone.setText(RCContacts.formatNumber(contact.phone));

        } else
            txtPhone.setText("--");

        if (contact.icon != null)
            imgProfPic.setImageBitmap(contact.icon);

        try {
            modContact c = dbC.getBlocked(contact.phone);
            if (c != null)
                if (c != null && c.wildcard != null && c.wildcard.trim().equals("X"))
                    imgBlock.setImageDrawable(getResources().getDrawable(R.drawable.ico_shield_selwild));
                else
                    imgBlock.setImageDrawable(getResources().getDrawable(R.drawable.ico_shield_sel));
            else
                imgBlock.setImageDrawable(getResources().getDrawable(R.drawable.ico_shieldblue));
        } catch (RCDBException e) {
            imgBlock.setImageDrawable(getResources().getDrawable(R.drawable.ico_shieldblue));
        }

        layIBlock.setTag("blnum" + String.valueOf(index));
        imgBlock.setTag("blnum" + contact.phone);
        layIBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idx = Integer.parseInt(view.getTag().toString().substring(5));
                modContact c = (modContact) ((MyBaseListLineAdapter)lstBlocked.getAdapter()).getContent().get(idx);
                ImageView im = (ImageView) view.findViewWithTag("blnum"+c.phone);
                dbContacts dbC = new dbContacts(myContext);
                String bl = c.phone;
                if (c.name != null && c.name.trim().length() > 0)
                    bl = c.name;
                if (im.getDrawable().getConstantState().equals
                        (getResources().getDrawable(R.drawable.ico_shieldblue).getConstantState())) {
                    try {
                        dbC.blockNum(c.phone, null);
                        Generic.replaceAllIMVDrawables(mainLayout, im.getTag().toString(), getResources().getDrawable(R.drawable.ico_shield_sel));
                        mainActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_block_on).toString() + " " + bl, 2);
                    } catch (RCDBException e) {

                    }
                } else {
                    try {
                        dbC.releaseNum(c.phone);
                        Generic.replaceAllIMVDrawables(mainLayout, im.getTag().toString(), getResources().getDrawable(R.drawable.ico_shieldblue));
                        mainActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_block_off).toString() + " " + bl, 2);
                    } catch (RCDBException e) {

                    }
                }
                im.refreshDrawableState();
            }
        });


        try {
            modContact c = dbC.getRec(contact.phone);
            if (c != null)
                imgRec.setImageDrawable(getResources().getDrawable(R.drawable.ico_mic_sel));
            else
                imgRec.setImageDrawable(getResources().getDrawable(R.drawable.ico_micblue));
        } catch (RCDBException e) {
            imgRec.setImageDrawable(getResources().getDrawable(R.drawable.ico_micblue));
        }

        layIRec.setTag("recnum" + String.valueOf(index));
        imgRec.setTag("recnum" + contact.phone);
        layIRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idx = Integer.parseInt(view.getTag().toString().substring(6));
                modContact c = (modContact) ((MyBaseListLineAdapter)lstBlocked.getAdapter()).getContent().get(idx);
                ImageView im = (ImageView) view.findViewWithTag("recnum"+c.phone);
                dbContacts dbC = new dbContacts(myContext);
                String bl = c.phone;
                if (c.name != null && c.name.trim().length() > 0)
                    bl = c.name;
                if (im.getDrawable().getConstantState().equals
                        (getResources().getDrawable(R.drawable.ico_micblue).getConstantState())) {
                    try {
                        dbC.recNum(c.phone, null);
                        Generic.replaceAllIMVDrawables(mainLayout, im.getTag().toString(), getResources().getDrawable(R.drawable.ico_mic_sel));
                        mainActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_rec_on).toString() + " " + bl, 2);
                    } catch (RCDBException e) {

                    }
                } else {
                    try {
                        dbC.norecNum(c.phone);
                        Generic.replaceAllIMVDrawables(mainLayout, im.getTag().toString(), getResources().getDrawable(R.drawable.ico_micblue));
                        mainActivity.mdiag.displayMessage(getActivity().getResources().getText(R.string.msg_rec_off).toString() + " " + bl, 2);
                    } catch (RCDBException e) {

                    }
                }
                im.refreshDrawableState();
            }
        });

        if (contact.dtview == null || contact.dtview.trim().length() <= 0) {
            imgNew.setVisibility(View.VISIBLE);
            try {
                dbBlock dbB = new dbBlock(getActivity().getBaseContext());
                dbB.setViewed(contact.phone, contact.dtcall);
            } catch (RCDBException e) {

            }
        }

        return lay;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CONTACTS_RES_REFRESH)
            loadBlocked(true);
    }

}
