package br.com.zpi.recall;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.widget.RemoteViews;

import br.com.zpi.recall.db.RCDBException;
import br.com.zpi.recall.db.dbBlock;
import br.com.zpi.recall.model.modContact;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
public class RCRecorderService extends Service {

    private static final String ACTION_IN = "android.intent.action.PHONE_STATE";
    private static final String ACTION_OUT = "android.intent.action.NEW_OUTGOING_CALL";
    private RCMediaRecorder rec = null;
    private static final int RECALL_SERVICE = 666;
    private NotificationManager mNotificationManager = null;
    private Notification notification = null;
    private IBinder LocalBinder = new LocalBinder();
    private static final String BLOCK_ALL = "br.com.zpi.recall.RCRecorderService.BLOCK_ALL";
    private static final String RECORD_ALL = "br.com.zpi.recall.RCRecorderService.RECORD_ALL";
    private static final String STOP = "br.com.zpi.recall.RCRecorderService.STOP";
    private static final String OPEN = "br.com.zpi.recall.RCRecorderService.OPEN";
    private SharedPreferences prefs = null;
    private RemoteViews contentView = null;
    private static int notcount = 0;

    //##############################################################################################
    // START LIKE UPDATER
    //##############################################################################################

    public class LocalBinder extends Binder {
        public RCRecorderService getService() {
            return RCRecorderService.this;
        }
    }


    @Override
    public IBinder onBind(Intent arg0) {
        return this.LocalBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        rec = new RCMediaRecorder();
        prefs = getBaseContext().getSharedPreferences("recall",
                Context.MODE_PRIVATE);

        final IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_OUT);
        filter.addAction(ACTION_IN);


    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        contentView = new RemoteViews(getPackageName(), R.layout.notif_recall);
        SharedPreferences.Editor editor = prefs.edit();

        // GET NOTIFIER ACTION
        if (intent != null) {
            String action = intent.getAction();

            if (action != null) {
                if (action.equals(BLOCK_ALL)) {
                    if (prefs.getString("blockall", "").trim().toUpperCase().equals("X")) {
                        editor.remove("blockall");
                        contentView.setImageViewResource(R.id.imgNBlock, R.drawable.ico_shieldall);
                        contentView.setTextViewText(R.id.txtInfoBlock, getBaseContext().getResources().getText(R.string.notif_stat_i));
                    } else {
                        contentView.setImageViewResource(R.id.imgNBlock, R.drawable.ico_shieldall_sel);
                        contentView.setTextViewText(R.id.txtInfoBlock, getBaseContext().getResources().getText(R.string.notif_stat_a));
                        editor.putString("blockall", "X");
                    }
                    editor.commit();
                    sendBroadcast(new Intent(RCConstants.REFRESHLAYOUT));
                } else if (action.equals(RECORD_ALL)) {
                    if (prefs.getString("recall", "").trim().toUpperCase().equals("X")) {
                        editor.remove("recall");
                        contentView.setImageViewResource(R.id.imgNRec, R.drawable.ico_micall);
                        contentView.setTextViewText(R.id.txtInfoRec, getBaseContext().getResources().getText(R.string.notif_stat_i));
                    } else {
                        editor.putString("recall", "X");
                        contentView.setImageViewResource(R.id.imgNRec, R.drawable.ico_micall_sel);
                        contentView.setTextViewText(R.id.txtInfoRec, getBaseContext().getResources().getText(R.string.notif_stat_a));
                    }
                    editor.commit();
                    sendBroadcast(new Intent(RCConstants.REFRESHLAYOUT));
                } else if (action.equals(OPEN)) {
                    Intent i = new Intent(getBaseContext(), MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                    sendBroadcast(it);
                } else if (action.equals(STOP)) {
                    stopForeground(true);
                    mNotificationManager.cancel(RECALL_SERVICE);
                    sendBroadcast(new Intent(RCConstants.RCCLOSE));
                    return START_NOT_STICKY;
                }
            }
        }

        refreshNotification();

        return START_NOT_STICKY;
    }


    public void refreshNotification() {
        notcount++;
        CharSequence tickerText = "BLIST";
        Notification.Builder builder = new Notification.Builder(getBaseContext());
        builder.setNumber(notcount);
        notification = builder.getNotification();
        notification.tickerText = tickerText;
        notification.icon = R.drawable.blisticon_t;
        long when = System.currentTimeMillis();
        notification.when = when;

        //set the button listeners
        setListeners(contentView);

        // Initialize notifier
        if (prefs.getString("blockall", "").trim().toUpperCase().equals("X")) {
            contentView.setImageViewResource(R.id.imgNBlock, R.drawable.ico_shieldall_sel);
            contentView.setTextViewText(R.id.txtInfoBlock, getBaseContext().getResources().getText(R.string.notif_stat_a));
        } else {
            contentView.setImageViewResource(R.id.imgNBlock, R.drawable.ico_shieldall);
            contentView.setTextViewText(R.id.txtInfoBlock, getBaseContext().getResources().getText(R.string.notif_stat_i));
        }

        if (prefs.getString("recall", "").trim().toUpperCase().equals("X")) {
            contentView.setImageViewResource(R.id.imgNRec, R.drawable.ico_micall_sel);
            contentView.setTextViewText(R.id.txtInfoRec, getBaseContext().getResources().getText(R.string.notif_stat_a));
        } else {
            contentView.setImageViewResource(R.id.imgNRec, R.drawable.ico_micall);
            contentView.setTextViewText(R.id.txtInfoRec, getBaseContext().getResources().getText(R.string.notif_stat_i));
        }

        notification.contentView = contentView;
        notification.flags = Notification.FLAG_NO_CLEAR;
        mNotificationManager.notify(RECALL_SERVICE, notification);
    }



    public void setListeners(RemoteViews view) {
        Intent block = new Intent(BLOCK_ALL);
        PendingIntent pRadio = PendingIntent.getService(RCRecorderService.this, 0, block, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.imgNBlock, pRadio);

        Intent record = new Intent(RECORD_ALL);
        PendingIntent pRec = PendingIntent.getService(RCRecorderService.this, 0, record, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.imgNRec, pRec);

        Intent stop = new Intent(STOP);
        PendingIntent pStop = PendingIntent.getService(RCRecorderService.this, 0, stop, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.imgNClose, pStop);

        Intent open = new Intent(OPEN);
        PendingIntent pOpen = PendingIntent.getService(RCRecorderService.this, 0, open, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.imgLogo, pOpen);
    }



}
