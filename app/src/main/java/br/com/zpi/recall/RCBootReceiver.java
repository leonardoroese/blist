package br.com.zpi.recall;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

public class RCBootReceiver extends BroadcastReceiver {
    private static final String BOOT = "android.intent.action.BOOT_COMPLETED";
    private String action = null;
    private static boolean booted = false;

    public RCBootReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        action = intent.getAction();

        if (action.equals(BOOT) && !booted) {
            Intent i = new Intent(context,
                    RCRecorderService.class);
            context.startService(i);
            booted = true;
        }
    }

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }
}
