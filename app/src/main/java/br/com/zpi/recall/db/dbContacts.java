package br.com.zpi.recall.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import br.com.zpi.recall.R;
import br.com.zpi.recall.model.modContact;

	/*
	 * Copyright 2018 Leonardo Germano Roese
	 *
	 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
	 * use this file except in compliance with the License. You may obtain a copy of
	 * the License at
	 *
	 * http://www.apache.org/licenses/LICENSE-2.0
	 *
	 * Unless required by applicable law or agreed to in writing, software
	 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
	 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
	 * License for the specific language governing permissions and limitations under
	 * the License.
	 */

public class dbContacts {

    private Context ctx = null;
    private DBHELPER dbh = null;


    public dbContacts(Context ctx) {
        this.ctx = ctx;
        this.dbh = new DBHELPER(this.ctx);
    }


    //----------------------------------------------------------------------------------------------
    // ADD BLOCK NUMBER ENTRY
    //----------------------------------------------------------------------------------------------

    public boolean blockNum(String number, String contactid) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_informnumber).toString());

        number = number.replaceAll("[^0-9*]", "");
        number = number.replaceAll(" ", "");
        SQLiteDatabase db = this.dbh.getWritableDatabase();

        ContentValues cv = new ContentValues(3);
        try{
            cv.put("number", URLEncoder.encode(number,"UTF-8"));
        }catch (Exception e){

        }
        if (contactid != null && contactid.trim().length() > 0)
            cv.put("contactid", contactid);
        else
            cv.put("contactid", " ");
        cv.put("dtadded", ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        try {
            if (db.insert("blocked", "number, contactid, dtadded", cv) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_insert).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_insert).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }

    //----------------------------------------------------------------------------------------------
    // ADD BLOCK NUMBER ENTRY
    //----------------------------------------------------------------------------------------------

    public boolean allowNum(String number, String contactid) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_informnumber).toString());

        number = number.replaceAll("[^0-9*]", "");
        number = number.replaceAll(" ", "");
        SQLiteDatabase db = this.dbh.getWritableDatabase();

        ContentValues cv = new ContentValues(3);
        try{
            cv.put("number", URLEncoder.encode(number,"UTF-8"));
        }catch (Exception e){

        }
        if (contactid != null && contactid.trim().length() > 0)
            cv.put("contactid", contactid);
        else
            cv.put("contactid", " ");
        cv.put("dtadded", ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        try {
            if (db.insert("allowed", "number, contactid, dtadded", cv) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_insert).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_insert).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }

    //----------------------------------------------------------------------------------------------
    // ADD RECORD NUMBER ENTRY
    //----------------------------------------------------------------------------------------------

    public boolean recNum(String number, String contactid) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_informnumber).toString());

        number = number.replaceAll("[^0-9*]", "");
        number = number.replaceAll(" ", "");
        SQLiteDatabase db = this.dbh.getWritableDatabase();

        ContentValues cv = new ContentValues(3);
        try{
            cv.put("number", URLEncoder.encode(number,"UTF-8"));
        }catch (Exception e){

        }
        if (contactid != null && contactid.trim().length() > 0)
            cv.put("contactid", contactid);
        else
            cv.put("contactid", " ");
        cv.put("dtadded", ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        try {
            if (db.insert("record", "number, contactid, dtadded", cv) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_insert).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_insert).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // REMOVE BLOCK NUMBER ENTRY
    //----------------------------------------------------------------------------------------------

    public boolean releaseNum(String number) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_informnumber).toString());

        SQLiteDatabase db = this.dbh.getWritableDatabase();
        number = number.replaceAll("[^0-9*]", "");
        number = number.replaceAll(" ", "");
        if (number.indexOf("*") < 0 && number.length() > 9)
            number = number.substring(number.length() - 9);
        try{
            number= URLEncoder.encode(number,"UTF-8");
        }catch (Exception e){

        }
        try {
            if (db.delete("blocked", "number LIKE ?", new String[]{"%"+number}) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_delete).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_delete).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }

    //----------------------------------------------------------------------------------------------
    // REMOVE allowed NUMBER ENTRY
    //----------------------------------------------------------------------------------------------

    public boolean delAllowedNum(String number) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_informnumber).toString());

        SQLiteDatabase db = this.dbh.getWritableDatabase();
        number = number.replaceAll("[^0-9*]", "");
        number = number.replaceAll(" ", "");
        if (number.indexOf("*") < 0 && number.length() > 9)
            number = number.substring(number.length() - 9);
        try{
            number= URLEncoder.encode(number,"UTF-8");
        }catch (Exception e){

        }
        try {
            if (db.delete("allowed", "number LIKE ?", new String[]{"%"+number}) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_delete).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_delete).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }

    //----------------------------------------------------------------------------------------------
    // REMOVE RECORD NUMBER ENTRY
    //----------------------------------------------------------------------------------------------

    public boolean norecNum(String number) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_informnumber).toString());

        SQLiteDatabase db = this.dbh.getWritableDatabase();
        number = number.replaceAll("[^0-9*]", "");
        number = number.replaceAll(" ", "");
        if (number.indexOf("*") < 0 && number.length() > 9)
            number = number.substring(number.length() - 9);
        try{
            number= URLEncoder.encode(number,"UTF-8");
        }catch (Exception e){

        }
        try {
            if (db.delete("record", "number LIKE ?", new String[]{"%"+number}) > 0) {
                db.close();
                return true;
            } else {
                db.close();
                throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_delete).toString());
            }
        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_delete).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // LIST ALL BLOCK NUMBERS
    //----------------------------------------------------------------------------------------------

    public modContact[] listBlocked(String filter, String order, int limit, int pageIn) throws RCDBException {
        SQLiteDatabase db = this.dbh.getReadableDatabase();

        String q = "SELECT * FROM blocked ";
        if(filter != null && filter.trim().length() > 0)
            try {
                q = q + " WHERE UPPER(number) LIKE '%" + URLEncoder.encode(filter, "UTF-8") + "%' ";
            }catch (Exception e){

            }
        if (order != null && order.trim().length() > 0)
            q = q + " ORDER BY " + order;
        else
            q = q + " ORDER BY dtadded DESC ";
        if (limit > 0)
            q = q + " LIMIT " + pageIn + ", " + limit;
        try {
            Cursor cur = db.rawQuery(q,null);
            if (cur != null && cur.moveToFirst()) {
                modContact[] outlist = new modContact[cur.getCount()];
                for (int i = 0; i < cur.getCount(); i++) {
                    outlist[i] = new modContact();
                    if (cur.getString(0) != null)
                        try{
                            outlist[i].phone = URLDecoder.decode(cur.getString(0),"UTF-8");
                        }catch (Exception w){

                        }
                    if (cur.getString(1) != null)
                        outlist[i].cid = cur.getString(1);
                    cur.moveToNext();
                }
                return outlist;
            } else {
                return null;
            }

        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_list).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // LIST ALLOWED NUMBERS
    //----------------------------------------------------------------------------------------------

    public modContact[] listAllowed(String filter, String order, int limit, int pageIn) throws RCDBException {
        SQLiteDatabase db = this.dbh.getReadableDatabase();

        String q = "SELECT * FROM allowed ";
        if(filter != null && filter.trim().length() > 0)
            try {
                q = q + " WHERE UPPER(number) LIKE '%" + URLEncoder.encode(filter, "UTF-8") + "%' ";
            }catch (Exception e){

            }
        if (order != null && order.trim().length() > 0)
            q = q + " ORDER BY " + order;
        else
            q = q + " ORDER BY dtadded DESC ";
        if (limit > 0)
            q = q + " LIMIT " + pageIn + ", " + limit;
        try {
            Cursor cur = db.rawQuery(q,null);
            if (cur != null && cur.moveToFirst()) {
                modContact[] outlist = new modContact[cur.getCount()];
                for (int i = 0; i < cur.getCount(); i++) {
                    outlist[i] = new modContact();
                    if (cur.getString(0) != null)
                        try{
                            outlist[i].phone = URLDecoder.decode(cur.getString(0),"UTF-8");
                        }catch (Exception w){

                        }
                    if (cur.getString(1) != null)
                        outlist[i].cid = cur.getString(1);
                    cur.moveToNext();
                }
                return outlist;
            } else {
                return null;
            }

        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_list).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // LIST ALL RECORD NUMBERS
    //----------------------------------------------------------------------------------------------

    public modContact[] listRec(String filter, String order, int limit, int pageIn) throws RCDBException {
        SQLiteDatabase db = this.dbh.getReadableDatabase();

        String q = "SELECT * FROM record ";
        if(filter != null && filter.trim().length() > 0)
            try {
                q = q + " WHERE UPPER(number) LIKE '%" + URLEncoder.encode(filter, "UTF-8") + "%' ";
            }catch (Exception e){

            }
        if (order != null && order.trim().length() > 0)
            q = q + " ORDER BY " + order;
        else
            q = q + " ORDER BY dtadded DESC ";
        if (limit > 0)
            q = q + " LIMIT " + pageIn + ", " + limit;

        try {
            Cursor cur = db.rawQuery(q,null);
            if (cur != null && cur.moveToFirst()) {
                modContact[] outlist = new modContact[cur.getCount()];
                for (int i = 0; i < cur.getCount(); i++) {
                    outlist[i] = new modContact();
                    if (cur.getString(0) != null)
                        try{
                            outlist[i].phone = URLDecoder.decode(cur.getString(0),"UTF-8");
                        }catch (Exception w){

                        }
                    if (cur.getString(1) != null)
                        outlist[i].cid = cur.getString(1);
                    cur.moveToNext();
                }
                return outlist;
            } else {
                return null;
            }

        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_list).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }


    //----------------------------------------------------------------------------------------------
    // GET SPECIFIC BLOCK NUMBER
    //----------------------------------------------------------------------------------------------

    public modContact getBlocked(String number) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_informnumber).toString());

        number = number.replaceAll("[^0-9]", "");
        number = number.replaceAll(" ", "");
        String originum = number;
        if (number.length() > 9)
            number = number.substring(number.length() - 9);

        SQLiteDatabase db = this.dbh.getReadableDatabase();

        try {
            String q = "SELECT * FROM blocked WHERE number LIKE '%" + number + "'";
            //prefix
            q = q + " OR ( number LIKE '%"+URLEncoder.encode("*","UTF-8")+"' AND INSTR('"+originum+"', SUBSTR(number,0,INSTR(number,'"+URLEncoder.encode("*","UTF-8")+"'))) = 1)";
            //sufix
            q = q + " OR ( number LIKE '"+URLEncoder.encode("*","UTF-8")+"%' AND INSTR('"+originum+"', SUBSTR(number,INSTR(number,'"+URLEncoder.encode("*","UTF-8")+"')+1,LENGTH(number))) >= (LENGTH(number) - LENGTH(INSTR(number,'"+URLEncoder.encode("*","UTF-8")+"')))) ";

            Cursor cur = db.rawQuery(q, null);
            if (cur != null && cur.moveToFirst()) {
                modContact outnum = new modContact();
                if (cur.getString(0) != null)
                    try{
                        outnum.phone = URLDecoder.decode(cur.getString(0),"UTF-8");
                    }catch (Exception w){

                    }
                if (cur.getString(1) != null)
                    outnum.cid = cur.getString(1);
                if(outnum.phone.indexOf("*") >= 0)
                    outnum.wildcard = "X";
                return outnum;
            } else {
                return null;
            }

        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_list).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }

    //----------------------------------------------------------------------------------------------
    // GET SPECIFIC ALLOWED NUMBER
    //----------------------------------------------------------------------------------------------

    public modContact getAllowed(String number) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_informnumber).toString());

        number = number.replaceAll("[^0-9]", "");
        number = number.replaceAll(" ", "");
        String originum = number;
        if (number.length() > 9)
            number = number.substring(number.length() - 9);

        SQLiteDatabase db = this.dbh.getReadableDatabase();
        String q = "SELECT * FROM allowed WHERE number LIKE '%" + number + "'";
        try {
            //prefix
            q = q + " OR ( number LIKE '%"+URLEncoder.encode("*","UTF-8")+"' AND INSTR('"+originum+"', SUBSTR(number,0,INSTR(number,'"+URLEncoder.encode("*","UTF-8")+"'))) = 1)";
            //sufix
            q = q + " OR ( number LIKE '"+URLEncoder.encode("*","UTF-8")+"%' AND INSTR('"+originum+"', SUBSTR(number,INSTR(number,'"+URLEncoder.encode("*","UTF-8")+"')+1,LENGTH(number))) >= (LENGTH(number) - LENGTH(INSTR(number,'"+URLEncoder.encode("*","UTF-8")+"')))) ";

            Cursor cur = db.rawQuery(q, null);
            if (cur != null && cur.moveToFirst()) {
                modContact outnum = new modContact();
                if (cur.getString(0) != null)
                    try{
                        outnum.phone = URLDecoder.decode(cur.getString(0),"UTF-8");
                    }catch (Exception w){

                    }
                if (cur.getString(1) != null)
                    outnum.cid = cur.getString(1);
                if(outnum.phone.indexOf("*") >= 0)
                    outnum.wildcard = "X";
                return outnum;
            } else {
                return null;
            }

        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_list).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }

    //----------------------------------------------------------------------------------------------
    // GET SPECIFIC RECORD NUMBER
    //----------------------------------------------------------------------------------------------

    public modContact getRec(String number) throws RCDBException {

        if (number == null || number.trim().length() <= 0)
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_informnumber).toString());

        number = number.replaceAll("[^0-9]", "");
        number = number.replaceAll(" ", "");
        if (number.length() > 9)
            number = number.substring(number.length() - 9);

        SQLiteDatabase db = this.dbh.getReadableDatabase();

        try {
            Cursor cur = db.rawQuery("SELECT * FROM record WHERE number LIKE '%" + number + "'", null);
            if (cur != null && cur.moveToFirst()) {
                modContact outnum = new modContact();
                if (cur.getString(0) != null)
                    try{
                        outnum.phone = URLDecoder.decode(cur.getString(0),"UTF-8");
                    }catch (Exception w){

                    }
                if (cur.getString(1) != null)
                    outnum.cid = cur.getString(1);
                if(outnum.phone.indexOf("*") >= 0)
                    outnum.wildcard = "X";
                return outnum;
            } else {
                return null;
            }

        } catch (Exception e) {
            if (db != null)
                db.close();
            throw new RCDBException(RCDBException.E_DATABASEERROR, ctx.getResources().getText(R.string.dbcontacts_msg_e_list).toString());
        } finally {
            if (db != null)
                db.close();
        }
    }

}
