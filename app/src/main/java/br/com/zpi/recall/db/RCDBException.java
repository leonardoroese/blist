package br.com.zpi.recall.db;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

public class RCDBException extends Exception {
    private String e_id = null;
    private String e_msg = null;

    public static final String E_GENERAL = "E";
    public static final String E_DATABASEERROR = "DB";
    public static final String E_DATABASEEXECERROR = "DBE";
    public static final String E_NOTCONNECTED = "NOT_CONNECTED";

    public RCDBException(String errid, String emsg) {
        this.e_id = errid;
        this.e_msg = emsg;
    }

    public String getE_id() {
        return e_id;
    }

    public void setE_id(String e_id) {
        this.e_id = e_id;
    }

    public String getE_msg() {
        return e_msg;
    }

    public void setE_msg(String e_msg) {
        this.e_msg = e_msg;
    }


}
