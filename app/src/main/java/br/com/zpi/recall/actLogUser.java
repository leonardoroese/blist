package br.com.zpi.recall;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import br.com.zpi.recall.db.RCDBException;
import br.com.zpi.recall.db.dbBlock;
import br.com.zpi.recall.db.dbContacts;
import br.com.zpi.recall.extendables.MyActivity;
import br.com.zpi.recall.extendables.MyBaseListLineAdapter;
import br.com.zpi.recall.model.modContact;

/*
 * Copyright 2018 Leonardo Germano Roese
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

public class actLogUser extends MyActivity {

    private ListView lstLogUser = null;
    private String user_id = null;
    private String user_name = null;
    private String user_phone = null;
    private String user_pic = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_log_user);

        TextView txtLogUserName = (TextView) findViewById(R.id.txtLogUserName);
        TextView txtLogUserPhone = (TextView) findViewById(R.id.txtLogUserPhone);
        ImageView imgLogUserProf = (ImageView) findViewById(R.id.imgLogUserProf);
        RelativeLayout layLogUserProf = (RelativeLayout) findViewById(R.id.layLogUserProf);
        lstLogUser = (ListView) findViewById(R.id.lstLogUser);

        if (getIntent().hasExtra("user_id"))
            user_id = getIntent().getStringExtra("user_id");

        if (getIntent().hasExtra("user_name"))
            user_name = getIntent().getStringExtra("user_name");

        if (getIntent().hasExtra("user_phone"))
            user_phone = getIntent().getStringExtra("user_phone");

        if (getIntent().hasExtra("user_pic"))
            user_pic = getIntent().getStringExtra("user_pic");


        if (user_pic != null && user_pic.trim().length() > 0) {

        } else {
            layLogUserProf.setVisibility(View.GONE);
        }

        if (user_phone == null || user_phone.trim().length() <= 0) {
            txtLogUserPhone.setText(user_phone);
            if (user_name == null || user_name.trim().length() <= 0)
                txtLogUserName.setText(getResources().getText(R.string.loguser_lit_number).toString() + " " + user_phone);
        }
        if (user_name == null || user_name.trim().length() <= 0)
            txtLogUserName.setText(user_name);


    }

    public void loadLogs() {
        dbBlock dbB = new dbBlock(getBaseContext());
        modContact[] logs = null;
        try {
            logs = (modContact[]) dbB.listLogs(user_phone);
        } catch (RCDBException e) {

        }
        if (logs != null && logs.length > 0) {
            ArrayList<modContact> a = new ArrayList<modContact>();
            for (modContact c : logs)
                if (c.phone != null && c.phone.trim().length() > 0)
                    a.add(c);

            MyBaseListLineAdapter adFiles = new MyBaseListLineAdapter(this, a) {
                @Override
                public View getView(int i, View view, ViewGroup viewGroup) {
                    modContact contact = (modContact) getContent().get(i);
                    String dtLog = "--";
                    String tmLog = "--";

                    try {
                        ZonedDateTime dt = ZonedDateTime.parse(contact.dtcall);
                        dtLog = dt.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                        tmLog = dt.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
                    } catch (Exception e) {

                    }
                    return drawLog(dtLog, tmLog);
                }
            };
            lstLogUser.setAdapter(adFiles);

        } else {

        }

    }

    public RelativeLayout drawLog(String date, String time) {
        LayoutInflater inf = getLayoutInflater();
        RelativeLayout lay = (RelativeLayout) inf.inflate(R.layout.line_log_user, null);
        dbContacts dbC = new dbContacts(this);
        dbBlock dbB = new dbBlock(this);

        TextView txtLogDate = (TextView) lay.findViewById(R.id.txtName);

        if (date != null && date.trim().length() > 0)
            txtLogDate.setText(date);
        else
            txtLogDate.setText("--");


        return lay;
    }

}
