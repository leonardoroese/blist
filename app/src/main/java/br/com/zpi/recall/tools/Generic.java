package br.com.zpi.recall.tools;

import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.StatFs;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by leonardo on 01/11/16.
 */

public class Generic {

    //----------------------------------------------------------------------------------------------
    // GET VIEWS BY TAG
    //----------------------------------------------------------------------------------------------

    public static ArrayList<View> getViewsByTag(ViewGroup root, String tag) {
        ArrayList<View> views = new ArrayList<View>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewsByTag((ViewGroup) child, tag));
            }

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj.equals(tag)) {
                views.add(child);
            }

        }
        return views;
    }

    //----------------------------------------------------------------------------------------------
    // REPLACE ALL IMAGEVIEW DRAWABLES
    //----------------------------------------------------------------------------------------------

    public static void replaceAllIMVDrawables(ViewGroup root, String tag, Drawable dr) {
        ArrayList<View> list = Generic.getViewsByTag(root, tag);
        if (list != null && list.size() > 0) {
            for (View v : list) {
                ((ImageView) v).setImageDrawable(dr);
            }

        }
    }

}
