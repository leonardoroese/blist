# BLIST - A blacklist/whitelist solution. Call recorder/blocker for Android #

This Application was available at Google Play Store in 2016 with 4.2 stars rating.
It is a very simple application created to allow Android users to block unwanted calls and also record ongoing calls. 
There is an AIDL (Telephony) implementation to capture incoming and outgoing audio from callstream or MIC (due to some coutries restrictive policies not allow call recordings, some manufactures disabled this resource from Android Custom ROMS). 

API 15 to 26
Jelly Bean - Oreo

![picture](blistscrplaystore.png)

Now this source is under Apache 2.0 License, enjoy!

This application extends some custom classes and uses fragments and TAB/PageViewer Navigation.


### COMPONENTS ###

Joda Time 2.8 dor date manipulation. (included)

Roboto Font (included assets for UI)

Using Internal SQLite DB implementing Android DB Helper for data persistence.

Sliding TabStrip Paging by (Andreas Stuetz) APACHE 2.0 License
https://github.com/astuetz/PagerSlidingTabStrip
andreas.stuetz@gmail.com


### PACKAGING DESCRIPTION ###

####/java br.com.zpi.recall####

Main package, as it was released on Play Store by www.zeropoint.com.br 

On the root we have Activities, Fragments and Receivers classes.

####DB####

SQLite Helper Implementation, tables definition, database data exchange classes.

####extendables####

Extendable classes or abstract classes for Activities, AsyncTasks and Dialogs.

####model###

Database Model Classes

####tools####

Tool classes for data conversion, metrics, format, Bitmap manipulation, Base64 converter, acess contacts from phone.



### LICENSE ###


Copyright 2017 Leonardo Germano Roese

Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.
	


